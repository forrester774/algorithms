import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QuickSort {
    public static void main(String[] args) {

        int[] ints = {10, 7, 2, 5, 4, 3, 9, 8, 1};
        System.out.println(Arrays.toString(quickSort(ints)));
    }

    public static int[] quickSort(int[] ints) {

        int length = ints.length;
        if (length <= 1) {
            return ints;
        }
        // vagy [0]
        int pivot = ints[0];

        List<Integer> left = new ArrayList<>();
        List<Integer> right = new ArrayList<>();

        for (int i = 1; i <= length - 1; i++) {
            if (ints[i] <= pivot) {
                left.add(ints[i]);
            } else {
                right.add(ints[i]);
            }
        }
        int[] leftSorted = quickSort(left.stream().mapToInt(Integer::intValue).toArray());
        int[] rightSorted = quickSort(right.stream().mapToInt(Integer::intValue).toArray());

        List<Integer> sortedArray = new ArrayList<>();

        for (int i : leftSorted) {
            sortedArray.add(i);
        }
        sortedArray.add(pivot);
        for (int i : rightSorted) {
            sortedArray.add(i);
        }

        return sortedArray.stream().mapToInt(Integer::intValue).toArray();
    }

}

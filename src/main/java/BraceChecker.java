public class BraceChecker {

    public static void main(String[] args) {
        System.out.println(isValid("[(])"));
    }

    public static boolean isValid(String braces) {
        String[] split = braces.split("");
        StringBuilder state = new StringBuilder();
        for (String character : split) {
            if (character.equals("(") || character.equals("[") || character.equals("{")) {
                state.append(character);
            } else {
                String current = state.toString();

                if (current.isBlank()) {
                    return false;
                }

                String last = current.substring(current.length() - 1);
                String remaining = current.substring(0, current.length() - 1);
                if (character.equals(")") && last.equals("(")) {
                    state = new StringBuilder(remaining);
                } else if(character.equals("}") && last.equals("{")) {
                    state = new StringBuilder(remaining);
                } else if(character.equals("]") && last.equals("[")) {
                    state = new StringBuilder(remaining);
                }
                else {
                    return false;
                }
            }
        }
        return state.toString().isBlank();
    }
}

/**
 * Write a function that takes a string of braces, and determines if the order of the braces is valid.
 * It should return true if the string is valid, and false if it's invalid.
 * <p>
 * This Kata is similar to the Valid Parentheses Kata, but introduces new characters: brackets [], and curly braces {}. Thanks to @arnedag for the idea!
 * <p>
 * All input strings will be nonempty, and will only consist of parentheses, brackets and curly braces: ()[]{}.
 * <p>
 * What is considered Valid?
 * A string of braces is considered valid if all braces are matched with the correct brace.
 * <p>
 * Examples
 * "(){}[]"   =>  True
 * "([{}])"   =>  True
 * "(}"       =>  False
 * "[(])"     =>  False
 * "[({})](]" =>  False
 */
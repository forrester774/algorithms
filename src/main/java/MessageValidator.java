import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MessageValidator {
    public static void main(String[] args) {
        System.out.println(isAValidMessage("3hey5hello2hi")); // true
        System.out.println(isAValidMessage("4code13hellocodewars")); // true
        System.out.println(isAValidMessage("3hey5hello2hi5")); // false
        System.out.println(isAValidMessage("code4hello5")); // false
        System.out.println(isAValidMessage("1a2bb3ccc4dddd5eeeee")); // true
        System.out.println(isAValidMessage("0")); // true
        System.out.println(isAValidMessage("")); // true
    }

    /**
     * In this kata, you have an input string and you should check whether it is a valid message. To decide that, you need to split the string by the numbers, and then compare the numbers with the number of characters in the following substring.
     * <p>
     * For example "3hey5hello2hi" should be split into 3, hey, 5, hello, 2, hi and the function should return true, because "hey" is 3 characters, "hello" is 5, and "hi" is 2; as the numbers and the character counts match, the result is true.
     * <p>
     * Notes:
     * <p>
     * Messages are composed of only letters and digits
     * Numbers may have multiple digits: e.g. "4code13hellocodewars" is a valid message
     * Every number must match the number of character in the following substring, otherwise the message is invalid: e.g. "hello5" and "2hi2" are invalid
     * If the message is an empty string, you should return true
     */

    public static boolean isAValidMessage(String message) {
        if (message.length() <= 1) {
            return true;
        }

        String[] split = message.split("");

        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

        List<Integer> numbers = Arrays.stream(split).filter(text -> pattern.matcher(text).matches()).map(Integer::valueOf).collect(Collectors.toList());
        List<String> texts = Arrays.stream(message.split("[0-9]")).filter(text -> !text.isBlank()).collect(Collectors.toList());

        for (int i = 0; i < numbers.size() - 1; i++) {
            if (numbers.get(i) != texts.get(i).length()) {
                return false;
            }
        }


        return true;
    }
}

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class DuplicateEncoder {

    public static void main(String[] args) {
        System.out.println(DuplicateEncoder.encode("Prespecialized")); // ")()())()(()()("
        System.out.println(DuplicateEncoder.encode("   ()(   ")); // "))))())))"
    }

    /**
     * The goal of this exercise is to convert a string to a new string where each character in the new string is
     * "(" if that character appears only once in the original string, or ")"
     * if that character appears more than once in the original string. Ignore capitalization when determining if a character is a duplicate.
     * <p>
     * Examples
     * "din"      =>  "((("
     * "recede"   =>  "()()()"
     * "Success"  =>  ")())())"
     * "(( @"     =>  "))(("
     * Notes
     * Assertion messages may be unclear about what they display in some languages.
     * If you read "...It Should encode XXX", the "XXX" is the expected result, not the input!
     */

    static String encode(String word) {

        Map<String, Integer> map = new HashMap<>();

        String[] split = word.split("");
        Arrays.stream(split).map(String::toLowerCase).forEach(character -> {
            Integer count = map.get(character);

            if(Objects.isNull(count)) {
                map.put(character, 1);
            } else {
                map.put(character, ++count);
            }

        });

        StringBuilder builder = new StringBuilder();

        for (String character : split) {
            if(map.get(character.toLowerCase()) > 1) {
                builder.append(")");
            } else {
                builder.append("(");
            }
        }
        return builder.toString();
    }
}

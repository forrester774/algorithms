import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SplitStrings {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(solution("")));
    }
    /**
     * Complete the solution so that it splits the string into pairs of two characters.
     * If the string contains an odd number of characters then it should replace the missing second character of the final pair with an underscore ('_').
     *
     * Examples:
     *
     * * 'abc' =>  ['ab', 'c_']
     * * 'abcdef' => ['ab', 'cd', 'ef']
     */

    public static String[] solution(String s) {
        List<String> result = new ArrayList<>();

        String[] split = s.split("(?<=\\G.{2})");

        for (String text : split) {
            if(text.length() != 2) {
                text = text + "_";
            }
            result.add(text);
        }

        return result.toArray(new String[0]);
    }
}

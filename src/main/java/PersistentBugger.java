public class PersistentBugger {
    public static void main(String[] args) {
        System.out.println(persistence(39)); // 3
        System.out.println(persistence(4)); // 0
        System.out.println(persistence(25)); // 2
        System.out.println(persistence(999)); // 4
    }

    /**
     * Write a function, persistence, that takes in a positive parameter num and returns its multiplicative persistence, which is the number of times you must multiply the digits in num until you reach a single digit.
     * <p>
     * For example (Input --> Output):
     * <p>
     * 39 --> 3 (because 3*9 = 27, 2*7 = 14, 1*4 = 4 and 4 has only one digit)
     * 999 --> 4 (because 9*9*9 = 729, 7*2*9 = 126, 1*2*6 = 12, and finally 1*2 = 2)
     * 4 --> 0 (because 4 is already a one-digit number)
     */

    public static int persistence(long n) {
        String[] digits = String.valueOf(n).split("");

        if (digits.length == 1) {
            return 0;
        }

        int numberOfMultiplications = 0;

        while (digits.length > 1) {
            long count = 1;
            for (String num : digits) {
                count *= Long.parseLong(num);
            }
            digits = String.valueOf(count).split("");
            numberOfMultiplications++;
        }


        return numberOfMultiplications;
    }
}

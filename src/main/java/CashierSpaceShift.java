import java.util.*;

public class CashierSpaceShift {

    public static void main(String[] args) {
        System.out.println(getOrder("milkshakepizzachickenfriescokeburgerpizzasandwichmilkshakepizza")); // "Burger Fries Chicken Pizza Pizza Pizza Sandwich Milkshake Milkshake Coke"
        System.out.println(getOrder("pizzachickenfriesburgercokemilkshakefriessandwich")); // "Burger Fries Fries Chicken Pizza Sandwich Milkshake Coke"
    }

    /**
     * Some new cashiers started to work at your restaurant.
     * <p>
     * They are good at taking orders, but they don't know how to capitalize words, or use a space bar!
     * <p>
     * All the orders they create look something like this:
     * <p>
     * "milkshakepizzachickenfriescokeburgerpizzasandwichmilkshakepizza"
     * <p>
     * The kitchen staff are threatening to quit, because of how difficult it is to read the orders.
     * <p>
     * Their preference is to get the orders as a nice clean string with spaces and capitals like so:
     * <p>
     * "Burger Fries Chicken Pizza Pizza Pizza Sandwich Milkshake Milkshake Coke"
     * <p>
     * The kitchen staff expect the items to be in the same order as they appear in the menu.
     * <p>
     * The menu items are fairly simple, there is no overlap in the names of the items:
     * <p>
     * 1. Burger
     * 2. Fries
     * 3. Chicken
     * 4. Pizza
     * 5. Sandwich
     * 6. Onionrings
     * 7. Milkshake
     * 8. Coke
     */

    public static String getOrder(String input) {

        Map<Integer, String> items = new HashMap<>();
        items.put(1, "burger");
        items.put(2, "fries");
        items.put(3, "chicken");
        items.put(4, "pizza");
        items.put(5, "sandwich");
        items.put(6, "onionrings");
        items.put(7, "milkshake");
        items.put(8, "coke");

        List<String> result = new ArrayList<>();

        for (String item : items.values()) {
            int index = input.indexOf(item);
            while(index>=0) {
                result.add(item.substring(0,1).toUpperCase() + item.substring(1));
                input = input.substring(0, index) +  input.substring(index + item.length());
                index = input.indexOf(item);
            }
        }

        return String.join(" ", result);
    }
}

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CountingDuplicates {
    public static void main(String[] args) {
        System.out.println(CountingDuplicates.duplicateCount("abcdeaBReturnsTwo")); // 2
        System.out.println(CountingDuplicates.duplicateCount("abcdea")); // 1
        System.out.println(CountingDuplicates.duplicateCount("indivisibility")); // 1

    }

    /**
     * Write a function that will return the count of distinct case-insensitive alphabetic characters and numeric digits that occur more than once in the input string. The input string can be assumed to contain only alphabets (both uppercase and lowercase) and numeric digits.
     * <p>
     * Example
     * "abcde" -> 0 # no characters repeats more than once
     * "aabbcde" -> 2 # 'a' and 'b'
     * "aabBcde" -> 2 # 'a' occurs twice and 'b' twice (`b` and `B`)
     * "indivisibility" -> 1 # 'i' occurs six times
     * "Indivisibilities" -> 2 # 'i' occurs seven times and 's' occurs twice
     * "aA11" -> 2 # 'a' and '1'
     * "ABBA" -> 2 # 'A' and 'B' each occur twice
     */
    public static int duplicateCount(String text) {
        Map<String, Integer> resultSet = new HashMap<>();

        Arrays.stream(text.toLowerCase().split("")).forEach(character -> {
            Integer numberOfTimes = resultSet.get(character);
            if (Objects.nonNull(numberOfTimes)) {
                resultSet.put(character, ++numberOfTimes);
            } else {
                resultSet.put(character, 0);
            }
        });

        return Long.valueOf(resultSet.values().stream().filter(num -> num >= 1).count()).intValue();
    }
}

import java.util.Arrays;

public class BubbleSort {

    public static void main(String[] args) {

        int[] ints = {10, 7, 5, 3, 2, 3, 8, 9, 1};
        bubbleSort(ints);

        System.out.println(Arrays.toString(ints));
    }

    public static void bubbleSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j + 1] < array[j]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }
}

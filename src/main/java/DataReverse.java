import java.util.*;
import java.util.stream.Collectors;

public class DataReverse {

    public static void main(String[] args) {
        int[] data1 = {1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0};
        int[] data2 = {1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1};
        System.out.println(Arrays.toString(DataReverse(data1)));
        System.out.println("expected: " + Arrays.toString(data2));
    }

    /**
     * A stream of data is received and needs to be reversed.
     * <p>
     * Each segment is 8 bits long, meaning the order of these segments needs to be reversed, for example:
     * <p>
     * 11111111  00000000  00001111  10101010
     * (byte1)   (byte2)   (byte3)   (byte4)
     * should become:
     * <p>
     * 10101010  00001111  00000000  11111111
     * (byte4)   (byte3)   (byte2)   (byte1)
     * The total number of bits will always be a multiple of 8.
     * <p>
     * The data is given in an array as such:
     * <p>
     * [1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,1,0,1,0,1,0]
     */

    public static int[] DataReverse(int[] data) {

        String bits = Arrays.stream(data).boxed().map(String::valueOf).collect(Collectors.joining());

        String[] split = bits.split("(?<=\\G.{8})");

        List<String> reversed = new ArrayList<>();
        for (int i = split.length; i > 0; i--) {
            reversed.add(split[i-1]);
        }

        return Arrays
                .stream(String.join("", reversed).split(""))
                .map(Integer::parseInt)
                .mapToInt(Integer::intValue)
                .toArray();
    }
}

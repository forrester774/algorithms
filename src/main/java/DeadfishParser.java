import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DeadfishParser {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(parse("iiisdoso"))); // new int[] {8, 64}
        System.out.println(Arrays.toString(parse("iiisdosodddddiso"))); // new int[] {8, 64, 3600}
    }

    /**
     * Write a simple parser that will parse and run Deadfish.
     *
     * Deadfish has 4 commands, each 1 character long:
     *
     * i increments the value (initially 0)
     * d decrements the value
     * s squares the value
     * o outputs the value into the return array
     * Invalid characters should be ignored.
     *
     * Deadfish.parse("iiisdoso") =- new int[] {8, 64};
     */

    public static int[] parse(String data) {
        String[] instructions = data.split("");

        List<Integer> result = new ArrayList<>();

        int value = 0;

        for (String instruction : instructions) {
            switch (instruction) {
                case "i" : value +=1; break;
                case "d" : value -=1; break;
                case "s" : value*= value; break;
                case "o" : result.add(value);
            }
        }

        return result.stream().mapToInt(Integer::intValue).toArray();
    }
}

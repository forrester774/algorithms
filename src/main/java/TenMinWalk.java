import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TenMinWalk {

    public static void main(String[] args) {
        System.out.println(isValid(new char[] {'n','s','n','s','n','s','n','s','n','s'})); // true
        System.out.println(isValid(new char[] {'w','e','w','e','w','e','w','e','w','e','w','e'})); // false
        System.out.println(isValid(new char[] {'w'})); // false
        System.out.println(isValid(new char[] {'n','n','n','s','n','s','n','s','n','s'})); // false
    }
    /**
     * You live in the city of Cartesia where all roads are laid out in a perfect grid.
     * You arrived ten minutes too early to an appointment, so you decided to take the opportunity to go for a short walk.
     * The city provides its citizens with a Walk Generating App on their phones --
     * everytime you press the button it sends you an array of one-letter strings representing directions to walk
     * (eg. ['n', 's', 'w', 'e']). You always walk only a single block for each letter (direction)
     * and you know it takes you one minute to traverse one city block, so create a function that will return true
     * if the walk the app gives you will take you exactly ten minutes (you don't want to be early or late!) and will,
     * of course, return you to your starting point. Return false otherwise.
     *
     * Note: you will always receive a valid array containing a random assortment of direction letters
     * ('n', 's', 'e', or 'w' only). It will never give you an empty array (that's not a walk, that's standing still!).
     */

    public static boolean isValid(char[] walk) {

        if(walk.length != 10) {
             return false;
        }

        String walkString = String.valueOf(walk);

        Map<String, Long> grouped = Arrays
                .stream(walkString.split(""))
                .collect(Collectors
                        .groupingBy(Function.identity(), Collectors.counting()));

        grouped.putIfAbsent("w", 0L);
        grouped.putIfAbsent("e", 0L);
        grouped.putIfAbsent("n", 0L);
        grouped.putIfAbsent("s", 0L);

        return grouped.get("w").equals(grouped.get("e")) && grouped.get("n").equals(grouped.get("s"));
    }
}

import java.lang.reflect.Array;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Stack<T> {

    private T[] array;
    private int size;
    private int elements = 0;


    public Stack(Class<T> tClass, int size) {
        this.size = size;
        this.array = (T[]) Array.newInstance(tClass, size);
    }

    public int getSize() {
        return elements;
    }

    public T peek() {
        if (elements > 0) {
            return this.array[elements - 1];
        }
        return null;
    }

    public T pop() {
        if (elements > 0) {
            T element = this.array[elements - 1];
            this.array[elements - 1] = null;
            elements--;
            return element;
        }
        return null;
    }

    public void push(T element) {

        if (size == elements + 1) {
            createNewArray(array, size * 2);
        }

        this.array[elements] = element;
        elements++;
    }

    private void createNewArray(T[] array, int newSize) {
        this.size = newSize;
        this.array = Arrays.copyOf(array, newSize);
    }
}

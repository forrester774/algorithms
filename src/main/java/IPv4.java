import java.util.Arrays;
import java.util.stream.Collectors;

public class IPv4 {
    public static void main(String[] args) {
        System.out.println(longToIP(2154959208L)); // 128.114.17.104
        System.out.println(longToIP(0)); // 0.0.0.0
        System.out.println(longToIP(2149583361L)); // 128.32.10.1
    }

    public static String longToIP(long ip) {
        String s = Long.toBinaryString(ip);
        String[] split = s.split("(?<=\\G.{8})");

        return Arrays
                .stream(split)
                .map(binary -> {
                    String string = String.valueOf(Integer.parseInt(binary, 2));
                    return string;
                })
                .collect(Collectors.joining("."));
    }
}

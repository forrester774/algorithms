import java.lang.reflect.Array;

public class Queue<T> {
    private int size;
    private Class<T> tClass;
    private int front = 0;
    private int tail = 0;
    private T[] array;

    public Queue(int size, Class<T> tClass) {
        this.size = size;
        this.tClass = tClass;
        array = (T[]) Array.newInstance(tClass, size);
    }

    public T peek() {
        if (array.length >= 1) {
            return array[front];
        }
        return null;
    }

    public T dequeue() {
        if (array.length >= 1) {
            T element = array[front];

            if(front != tail) {
                front++;
            }
            return element;
        }

        return null;
    }

    public void queue(T element) {
        if (array.length < size) {
            array[tail] = element;
            tail++;
        } else {
            T[] tempArray = (T[]) Array.newInstance(tClass, size * 2);

            for (int i = 0; i <= array.length - 1; i++) {
                tempArray[i] = array[i];
            }
            tempArray[tail] = element;
            size *= 2;
            tail++;
            this.array = tempArray;
        }
    }
}

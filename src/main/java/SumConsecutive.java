import java.util.ArrayList;
import java.util.List;

public class SumConsecutive {

    public static void main(String[] args) {

    }

    /**
     * You are given a list/array which contains only integers (positive and negative).
     * Your job is to sum only the numbers that are the same and consecutive. The result should be one list.
     * <p>
     * Extra credit if you solve it in one line. You can assume there is never an empty list/array and there will always be an integer.
     * <p>
     * Same meaning: 1 == 1
     * <p>
     * 1 != -1
     * <p>
     * #Examples:
     * <p>
     * [1,4,4,4,0,4,3,3,1] # should return [1,12,0,4,6,1]
     * <p>
     * """So as you can see sum of consecutives 1 is 1
     * sum of 3 consecutives 4 is 12
     * sum of 0... and sum of 2
     * consecutives 3 is 6 ..."""
     * <p>
     * [1,1,7,7,3] # should return [2,14,3]
     * [-5,-5,7,7,12,0] # should return [-10,14,12,0]
     */

    public static List<Integer> sumConsecutives(List<Integer> s) {

        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < s.size(); i++) {
            int sum = s.get(i);
            for (int j = i + 1; j < s.size(); j++) {
                if (s.get(i) == s.get(j)) {
                    sum += j;
                } else {
                    break;
                }
            }
            result.add(sum);
        }

        return result;
    }
}

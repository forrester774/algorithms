import java.util.HashMap;
import java.util.Map;

public class Fibonacci {

    public static void main(String[] args) {
        System.out.println("Recursive fibonacci exaple.");

        System.out.println(fibonacciRecursive(7));

        System.out.println("-----------------------------------");

        System.out.println(fibonacciRecursiveMemoized(7, new HashMap<Integer, Integer>()));

        System.out.println("-----------------------------------");

        System.out.println(fibonacciIterative(7));

    }

    private static int fibonacciRecursive(int index) {
        if (index <= 1) {
            return index;
        }
        return fibonacciRecursive(index - 1) + fibonacciRecursive(index - 2);
    }

    private static int fibonacciRecursiveMemoized(int index, Map<Integer, Integer> memo) {
        if (index <= 1) {
            return index;
        }
        if (memo.containsKey(index)) {
            return memo.get(index);
        }
        int value = fibonacciRecursiveMemoized(index - 1, memo) + fibonacciRecursiveMemoized(index - 2, memo);
        memo.put(index, value);
        return value;

    }

    private static int fibonacciIterative(int index) {
        int first = 0;
        int second = 1;

        for (int i = 2; i <= index; i++) {
            int temp = first + second;
            first = second;
            second = temp;
        }
        return second;
    }
}

public class QueueDriver {
    public static void main(String[] args) {
        Queue<Integer> queue = new Queue<>(5, Integer.class);

        queue.queue(5);
        queue.queue(6);
        queue.queue(7);

        System.out.println(queue.peek());
        System.out.println(queue.peek());

        System.out.println("--------------------");

        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());

        System.out.println("------------------");
        queue.queue(1);
        queue.queue(2);
        queue.queue(3);
        queue.queue(4);
        queue.queue(5);
        queue.queue(6);
        queue.queue(7);
        queue.queue(8);

        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());
        System.out.println(queue.dequeue());



    }
}

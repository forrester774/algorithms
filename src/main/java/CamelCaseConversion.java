public class CamelCaseConversion {

    public static void main(String[] args) {
        System.out.println(toCamelCase("the_Stealth_Warrior")); // theStealthWarrior
        System.out.println(toCamelCase("the-Stealth-Warrior")); // theStealthWarrior
    }

    /**
     * Complete the method/function so that it converts dash/underscore delimited words into camel casing. The first word within the output should be capitalized only if the original word was capitalized (known as Upper Camel Case, also often referred to as Pascal case).
     * <p>
     * Examples
     * "the-stealth-warrior" gets converted to "theStealthWarrior"
     * "The_Stealth_Warrior" gets converted to "TheStealthWarrior"
     */

    static String toCamelCase(String s) {

        String[] split = s.split("(-|_)");
        StringBuilder builder = new StringBuilder(split[0]);
        for (int i = 1; i < split.length; i++) {
            builder.append(split[i].substring(0,1).toUpperCase())
                    .append(split[i].substring(1));
        }
        return builder.toString();
    }

}

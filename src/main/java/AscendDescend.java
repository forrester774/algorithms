public class AscendDescend {

    public static void main(String[] args) {
        System.out.println(AscendDescend.ascendDescend(10, -5, -1)); // "12321"
        System.out.println(AscendDescend.ascendDescend(14, 0, 2)); // "01210121012101"
        System.out.println(AscendDescend.ascendDescend(11, 5, 9)); // 56789876567
    }

    /**
     * You are given three integer inputs: length, minimum, and maximum.
     * <p>
     * Return a string that:
     * <p>
     * Starts at minimum
     * Ascends one at a time until reaching the maximum, then
     * Descends one at a time until reaching the minimum
     * repeat until the string is the appropriate length
     * <p>
     * Examples:
     * <p>
     * length: 5, minimum: 1, maximum: 3   ==>  "12321"
     * length: 14, minimum: 0, maximum: 2  ==>  "01210121012101"
     * length: 11, minimum: 5, maximum: 9  ==>  "56789876567"
     * <p>
     * Notes:
     * <p>
     * length will always be non-negative
     * negative numbers can appear for minimum and maximum values
     * hyphens/dashes ("-") for negative numbers do count towards the length
     * the resulting string must be truncated to the exact length provided
     * return an empty string if maximum < minimum or length == 0
     * minimum and maximum can equal one another and result in a single number repeated for the length of the string
     *
     * @param length
     * @param minimum
     * @param maximum
     * @return
     */
    public static String ascendDescend(int length, int minimum, int maximum) {
        if (minimum > maximum || length == 0) {
            return "";
        }

        StringBuilder builder = new StringBuilder("" + minimum);
        if (minimum == maximum) {
            for (int i = 1; i < length; i++) {
                builder.append(minimum);
            }
            return builder.toString();
        }


        boolean ascending = true;
        int currentVal = minimum + 1;
        for (int i = 1; i < length; i++) {
            builder.append(currentVal);

            if (currentVal == maximum || currentVal == minimum) {
                ascending = !ascending;
            }

            if (ascending) {
                currentVal++;
            } else {
                currentVal--;
            }

        }
        return builder.toString();
    }
}
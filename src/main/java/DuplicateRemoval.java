import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DuplicateRemoval {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(dup(new String[]{"ccooddddddewwwaaaaarrrrsssss", "piccaninny", "hubbubbubboo"}))); // "codewars","picaniny","hubububo"
        System.out.println(Arrays.toString(dup(new String[]{"abracadabra", "allottee", "assessee"}))); // abracadabra","alote","asese
        System.out.println(Arrays.toString(dup(new String[]{"kelless", "keenness"}))); // "keles","kenes"

    }

    /**
     * In this Kata, you will be given an array of strings and your task is to remove all consecutive duplicate letters from each string in the array.
     *
     * For example:
     *
     * dup(["abracadabra","allottee","assessee"]) = ["abracadabra","alote","asese"].
     *
     * dup(["kelless","keenness"]) = ["keles","kenes"].
     *
     * Strings will be lowercase only, no spaces. See test cases for more examples.
     * @param arr
     * @return
     */
    public static String[] dup(String[] arr){

        if(arr.length == 0) {
            return new String[]{};
        }
        List<String> result = new ArrayList<>();

        for (String word : arr) {
            StringBuilder builder = new StringBuilder();
            String[] split = word.split("");
            String actual = split[0];
            builder.append(actual);
            for (int i = 1; i < split.length; i++) {
                if(!split[i].equals(actual)){
                    builder.append(split[i]);
                    actual = split[i];
                }
            }
            result.add(builder.toString());
        }

        return result.toArray(new String[0]);
    }
}

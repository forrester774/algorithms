import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;

public class InnerContentSort {

    public static void main(String[] args) {
        System.out.println(sortTheInnerContent("sort the inner content in descending order"));// srot the inner ctonnet in dsnnieedcg oredr"
        System.out.println(sortTheInnerContent("wait for me")); // "wiat for me"
        System.out.println(sortTheInnerContent("this kata is easy")); // "tihs ktaa is esay"
    }

    /**
     * You have to sort the inner content of every word of a string in descending order.
     * <p>
     * The inner content is the content of a word without first and the last char.
     * <p>
     * Some examples:
     * <p>
     * "sort the inner content in descending order"  -->  "srot the inner ctonnet in dsnnieedcg oredr"
     * "wait for me"        -->  "wiat for me"
     * "this kata is easy"  -->  "tihs ktaa is esay"
     * Words are made up of lowercase letters.
     * <p>
     * The string will never be null and will never be empty. In C/C++ the string is always nul-terminated.
     */

    public static String sortTheInnerContent(String words) {
        return Arrays.stream(words.split(" "))
                .map(word -> {
                    if (word.length() < 3) {
                        return word;
                    }
                    return word.charAt(0) + Arrays.stream(word.substring(1, word.length() - 1).split(""))
                            .sorted(Comparator.reverseOrder())
                            .collect(Collectors.joining("")) + word.charAt(word.length() - 1);
                })
                .collect(Collectors.joining(" "));
    }
}

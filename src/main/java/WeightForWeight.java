import java.util.*;
import java.util.stream.Collectors;

public class WeightForWeight {

    public static void main(String[] args) {
        System.out.println(orderWeight("103 123 4444 99 2000")); // "2000 103 123 4444 99"
        System.out.println(orderWeight("2000 10003 1234000 44444444 9999 11 11 22 123")); // "11 11 2000 10003 22 123 1234000 44444444 9999"

    }

    /**
     * My friend John and I are members of the "Fat to Fit Club (FFC)".
     * John is worried because each month a list with the weights of members is published and each month he is the last on the list which means he is the heaviest.
     * <p>
     * I am the one who establishes the list so I told him:
     * "Don't worry any more, I will modify the order of the list".
     * It was decided to attribute a "weight" to numbers. The weight of a number will be from now on the sum of its digits.
     * <p>
     * For example 99 will have "weight" 18, 100 will have "weight" 1 so in the list 100 will come before 99.
     * <p>
     * Given a string with the weights of FFC members in normal order can you give this string ordered by "weights" of these numbers?
     * <p>
     * Example:
     * "56 65 74 100 99 68 86 180 90" ordered by numbers weights becomes:
     * <p>
     * "100 180 90 56 65 74 68 86 99"
     * When two numbers have the same "weight", let us class them as if they were strings (alphabetical ordering) and not numbers:
     * <p>
     * 180 is before 90 since, having the same "weight" (9), it comes before as a string.
     * <p>
     * All numbers in the list are positive numbers and the list can be empty.
     * <p>
     * Notes
     * it may happen that the input string have leading, trailing whitespaces and more than a unique whitespace between two consecutive numbers
     */

    public static String orderWeight(String strng) {

        String[] split = strng.split(" ");

        Map<Integer, List<String>> result = new TreeMap<>();

        for (String number : split) {
            int res = 0;
            String[] digits = number.split("");
            for (String digit : digits) {
                res += Integer.parseInt(digit);
            }
            result.computeIfAbsent(res, key -> new ArrayList<>()).add(number);
        }

        return result.entrySet().stream().flatMap(entry -> {
            return entry.getValue().stream().sorted(Comparator.comparing(String::valueOf));
        }).collect(Collectors.joining(" "));

    }
}

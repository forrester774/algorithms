import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class YourOrderPlease {

    public static void main(String[] args) {

        System.out.println(order("is2 Thi1s T4est 3a")); // Thi1s is2 3a T4est
        System.out.println(order("4of Fo1r pe6ople g3ood th5e the2")); // Fo1r the2 g3ood 4of th5e pe6ople

    }

    /**
     * Your task is to sort a given string. Each word in the string will contain a single number. This number is the position the word should have in the result.
     *
     * Note: Numbers can be from 1 to 9. So 1 will be the first word (not 0).
     *
     * If the input string is empty, return an empty string. The words in the input String will only contain valid consecutive numbers.
     *
     * Examples
     * "is2 Thi1s T4est 3a"  -->  "Thi1s is2 3a T4est"
     * "4of Fo1r pe6ople g3ood th5e the2"  -->  "Fo1r the2 g3ood 4of th5e pe6ople"
     * ""  -->  ""
     */

    public static String order(String words) {

        if(Objects.isNull(words) || words.length() == 0) {
             return "";
        }

        Map<Integer, String> map = new TreeMap<>();

        for (String word : words.split(" ")) {
            String clean = word.replaceAll("\\D+","");
            map.put(Integer.valueOf(clean), word);
        }

        return String.join(" ", map.values());
    }



}

import java.util.ArrayList;
import java.util.List;

public class SelectionSort {

    public static void main(String[] args) {
        List<Integer> array = new ArrayList<>();

        array.add(10);
        array.add(6);
        array.add(8);
        array.add(2);
        array.add(4);
        array.add(3);
        array.add(1);

        System.out.println(selectionSort(array));
    }

    public static List<Integer> selectionSort(List<Integer> array) {
        List<Integer> result = new ArrayList<>();
        List<Integer> copyArray = new ArrayList<>(array);
        for (int i = 0; i <= array.size() - 1; i++) {
            int indexOfSmallest = getIndexOfSmallest(copyArray);
            result.add(copyArray.get(indexOfSmallest));
            copyArray.remove(indexOfSmallest);
        }

        return result;
    }

    private static int getIndexOfSmallest(List<Integer> array) {
        int index = 0;
        int min = array.get(0);
        for (int i = 1; i <= array.size() - 1; i++) {
            if (min > array.get(i)) {
                index = i;
                min = array.get(i);
            }
        }

        return index;
    }
}

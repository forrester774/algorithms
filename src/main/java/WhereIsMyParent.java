import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class WhereIsMyParent {

    public static void main(String[] args) {
        basicTests();
    }

    private static void test(final String expected, final String input) {
        System.out.println(WhereIsMyParent.findChildren(input));
    }

    public static void basicTests() {
        test("AaBb", "abBA");
        test("AaaaaaZzzz", "AaaaaZazzz");
        test("AaBbbCcc", "CbcBcbaA");
        test("FfUuuuXx", "xXfuUuuF");
        test("", "");
    }

    /**
     * Mothers arranged a dance party for the children in school.
     * At that party, there are only mothers and their children.
     * All are having great fun on the dance floor when suddenly all the lights went out.
     * It's a dark night and no one can see each other.
     * But you were flying nearby and you can see in the dark and have ability to teleport people anywhere you want.
     *
     * Legend:
     * -Uppercase letters stands for mothers, lowercase stand for their children, i.e. "A" mother's children are "aaaa".
     * -Function input: String contains only letters, uppercase letters are unique.
     * Task:
     * Place all people in alphabetical order where Mothers are followed by their children, i.e. "aAbaBb" => "AaaBbb".
     */

    static String findChildren(final String text) {
        if(Objects.isNull(text) || text.isBlank()) {
            return "";
        }

        String sorted = Arrays.stream(text.split(""))
                .map(String::toLowerCase)
                .sorted()
                .collect(Collectors.joining(""));

        StringBuilder builder = new StringBuilder(sorted);
        builder.setCharAt(0,Character.toUpperCase(builder.charAt(0)));

        String[] split = sorted.split("");

        for (int i = 0; i < split.length-1; i++) {
            if(!split[i].toLowerCase().equals(split[i+1])) {
                builder
                        .setCharAt(i+1, Character.toUpperCase(builder.charAt(i+1)));
            }
        }

        return builder.toString();
    }
}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ArrayDiff {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(arrayDiff(new int[]{1, 2}, new int[]{1}))); // new int[] {2}
        System.out.println(Arrays.toString(arrayDiff(new int[]{1, 2, 2}, new int[]{1}))); // new int[] {2,2}
        System.out.println(Arrays.toString(arrayDiff(new int[]{1, 2, 2}, new int[]{2}))); // new int[] {1}
        System.out.println(Arrays.toString(arrayDiff(new int[]{1, 2, 2}, new int[]{}))); // new int[] {1,2,2}
        System.out.println(Arrays.toString(arrayDiff(new int[]{}, new int[]{1, 2}))); // new int[] {}
    }

    /**
     * Your goal in this kata is to implement a difference function, which subtracts one list from another and returns the result.
     *
     * It should remove all values from list a, which are present in list b keeping their order.
     *
     * Kata.arrayDiff(new int[] {1, 2}, new int[] {1}) => new int[] {2}
     * If a value is present in b, all of its occurrences must be removed from the other:
     *
     * Kata.arrayDiff(new int[] {1, 2, 2, 2, 3}, new int[] {2}) => new int[] {1, 3}
     */

    public static int[] arrayDiff(int[] a, int[] b) {
        List<Integer> result = new ArrayList<>();
        List<Integer> bList = Arrays.stream(b).boxed().collect(Collectors.toList());

        for (int num : a) {
            if(!bList.contains(num)) {
                result.add(num);
            }
        }
        return result
                .stream()
                .mapToInt(Integer::intValue)
                .toArray();
    }
}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ArraysAndStrings {

    public static void main(String[] args) {

        System.out.println(doesStringHaveOnlyUniqueValue("abcdefgh"));
        System.out.println(doesStringHaveOnlyUniqueValue("aabcdefgh"));

        System.out.println("------------------------------------");


        System.out.println(areStringsPermutation("god", "dog"));
        System.out.println(areStringsPermutation(null, "dog"));
        System.out.println(areStringsPermutation(null, null));
        System.out.println(areStringsPermutation("null", null));
        System.out.println("----------------------------------------");

        System.out.println(URLify("ASD JKL QWERTY"));

        System.out.println("-------------------------");

        System.out.println(Arrays.toString(averages(new int[]{1, 3, 5, 1, -10, 15})));


        System.out.println("-------------------");
        System.out.println(spinWords("This sentence is a sentence"));

        System.out.println("---------------------------");


        System.out.println(findIt(new int[]{1, 2, 2, 3, 3, 3, 4, 3, 3, 3, 2, 2, 1}));

        System.out.println("----------------------");
        System.out.println(Arrays.toString(inArray(new String[]{"arp", "live", "strong"}, new String[]{"lively", "alive", "harp", "sharp", "armstrong"})));

        System.out.println("-------------------");
        System.out.println(greedy(new int[]{5, 1, 3, 4, 1}));


        System.out.println("---------------------------");
        System.out.println(Arrays.toString(dirReduc(new String[]{"NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH"})));

        System.out.println("---------------------------------");


        System.out.println(Arrays.toString(deleteN(new int[]{20, 37, 20, 21}, 1)));
        System.out.println(Arrays.toString(deleteN(new int[]{1, 1, 3, 3, 7, 2, 2, 2, 2}, 3)));

    }

    public static boolean doesStringHaveOnlyUniqueValue(String string) {
        if (Objects.isNull(string) || string.isBlank()) {
            return false;
        }

        Set<String> set = Arrays.stream(string.split("")).collect(Collectors.toSet());
        List<String> list = Arrays.stream(string.split("")).collect(Collectors.toList());

        return set.size() == list.size();
    }

    public static boolean areStringsPermutation(String string1, String string2) {
        if (Objects.isNull(string1) && Objects.nonNull(string2)) {
            return false;
        }
        if (Objects.nonNull(string1) && Objects.isNull(string2)) {
            return false;
        }

        if (Objects.isNull(string1) && Objects.isNull(string2)) {
            return true;
        }
        List<String> collect1 = Arrays.stream(string1.trim().split("")).sorted().collect(Collectors.toList());
        List<String> collect2 = Arrays.stream(string2.trim().split("")).sorted().collect(Collectors.toList());

        String joined1 = String.join("", collect1);
        String joined2 = String.join("", collect2);

        return Objects.equals(joined1, joined2);
    }

    public static String URLify(String string) {
        if (Objects.isNull(string)) {
            return null;
        }
        return string.replace(" ", "%20");
    }

    /**
     * #Get the averages of these numbers
     * <p>
     * Write a method, that gets an array of integer-numbers and return an array of the averages of each integer-number and his follower, if there is one.
     * <p>
     * Example:
     * <p>
     * Input:  [ 1, 3, 5, 1, -10]
     * Output:  [ 2, 4, 3, -4.5]
     * If the array has 0 or 1 values or is null, your method should return an empty array.
     **/
    public static double[] averages(int[] numbers) {

        if (Objects.isNull(numbers) || numbers.length == 0 || numbers.length == 1) {
            return new double[0];
        }
        double[] averages = new double[numbers.length - 1];

        for (int i = 0; i <= numbers.length - 1; i++) {
            if (i + 1 < numbers.length) {
                averages[i] = ((numbers[i] + numbers[i + 1]) / 2.0);
            }
        }

        return averages;
    }

    /**
     * Write a function that takes in a string of one or more words, and returns the same string, but with all five or more letter words reversed (like the name of this kata).
     * <p>
     * Strings passed in will consist of only letters and spaces.
     * Spaces will be included only when more than one word is present.
     *
     * @param sentence
     * @return
     */
    public static String spinWords(String sentence) {
        if (sentence == null) {
            return null;
        }

        String[] listOfStrings = (sentence.split(" "));

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i <= listOfStrings.length - 1; i++) {
            String string = listOfStrings[i];
            if (string.length() >= 5) {
                StringBuilder stringBuilder1 = new StringBuilder(string).reverse();

                stringBuilder.append(stringBuilder1);
            } else {
                stringBuilder.append(string);
            }

            if (i <= listOfStrings.length - 2) {
                stringBuilder.append(" ");
            }
        }

        return stringBuilder.toString();
    }

    /**
     * Given an array of integers, find the one that appears an odd number of times.
     * <p>
     * There will always be only one integer that appears an odd number of times.
     *
     * @param a
     * @return
     */
    public static int findIt(int[] a) {
        HashMap<Integer, Integer> map = new HashMap<>();

        for (int number : a) {
            Integer integer = map.get(number);
            if (integer == null) {
                map.put(number, 1);
            } else {
                integer += 1;
                map.put(number, integer);
            }
        }

        return map.entrySet().stream().filter(entry -> entry.getValue() % 2 != 0).findFirst().get().getKey();
    }

    /**
     * Given two arrays of strings a1 and a2 return a sorted array r in lexicographical order of the strings of a1 which are substrings of strings of a2.
     *
     * @param array1
     * @param array2
     * @return
     */
    public static String[] inArray(String[] array1, String[] array2) {
        List<String> contains = new ArrayList<>();

        for (String string : array1) {
            for (String sub : array2) {
                if (sub.contains(string)) {
                    contains.add(string);
                    break;
                }
            }
        }

        return contains.stream().sorted().toArray(String[]::new);
    }

    /**
     * Greed is a dice game played with five six-sided dice. Your mission, should you choose to accept it, is to score a throw according to these rules. You will always be given an array with five six-sided dice values.
     * <p>
     * Three 1's => 1000 points
     * Three 6's =>  600 points
     * Three 5's =>  500 points
     * Three 4's =>  400 points
     * Three 3's =>  300 points
     * Three 2's =>  200 points
     * One   1   =>  100 points
     * One   5   =>   50 point
     * A single die can only be counted once in each roll. For example, a given "5" can only count as part of a triplet (contributing to the 500 points) or as a single 50 points, but not both in the same roll.
     * <p>
     * Example scoring
     * <p>
     * Throw       Score
     * ---------   ------------------
     * 5 1 3 4 1   250:  50 (for the 5) + 2 * 100 (for the 1s)
     * 1 1 1 3 1   1100: 1000 (for three 1s) + 100 (for the other 1)
     * 2 4 4 5 4   450:  400 (for three 4s) + 50 (for the 5)
     *
     * @param dice
     * @return
     */
    public static int greedy(int[] dice) {
        AtomicInteger score = new AtomicInteger();
        Map<Integer, Integer> map = new HashMap<>();
        for (int number : dice) {
            if (map.containsKey(number)) {
                Integer integer = map.get(number);
                integer += 1;
                map.put(number, integer);
            } else {
                map.put(number, 1);
            }

        }

        map.forEach((key, value) -> {
            int remainingUnused = value;

            if (value >= 3) {
                remainingUnused = value - 3;

                if (key == 1) {
                    score.addAndGet(1000);
                } else {
                    score.addAndGet(key * 100);
                }
            }

            if (key == 1) {
                score.addAndGet(remainingUnused * 100);
            } else if (key == 5) {
                score.addAndGet(remainingUnused * 50);
            }
        });

        return score.get();
    }


    /**
     * Write a function dirReduc which will take an array of strings and returns an array of strings with the needless directions removed (W<->E or S<->N side by side).
     *
     * @param arr
     * @return
     */
    public static String[] dirReduc(String[] arr) {
        int length = arr.length;
        if (length <= 1) {
            return arr;
        }

        String[] array = Arrays.copyOf(arr, arr.length);
        int currentSize = array.length;
        int previousSize = 0;
        while (currentSize != previousSize) {

            List<String> strings = new ArrayList<>();
            previousSize = array.length;
            for (int i = 1; i <= array.length - 1; i++) {
                Directions current = Directions.getDirections(array[i - 1]);
                Directions next = Directions.getDirections(array[i]);
                if (current.getDirection().equals(next.getOpposite())) {
                    for (int j = 0; j <= array.length - 1; j++) {
                        if (j != i && j != i - 1) {
                            strings.add(array[j]);
                        }
                    }
                    array = strings.toArray(String[]::new);
                    currentSize = array.length;
                    break;
                }
            }
        }
        return array;
    }

    public enum Directions {
        NORTH("north", "south"),
        SOUTH("south", "north"),
        EAST("east", "west"),
        WEST("west", "east");

        private final String direction;
        private final String opposite;

        Directions(String direction, String opposite) {
            this.direction = direction;
            this.opposite = opposite;
        }

        public static Directions getDirections(String direction) {
            switch (direction.toLowerCase()) {
                case "north":
                    return NORTH;
                case "south":
                    return SOUTH;
                case "east":
                    return EAST;
                case "west":
                    return WEST;
                default:
                    throw new RuntimeException();
            }
        }

        public String getDirection() {
            return direction;
        }

        public String getOpposite() {
            return opposite;
        }

    }

    /**
     * Write a function that takes a positive integer and returns the next smaller positive integer containing the same digits.
     *
     * For example:
     *
     * nextSmaller(21) == 12
     * nextSmaller(531) == 513
     * nextSmaller(2071) == 2017
     * Return -1 (for Haskell: return Nothing, for Rust: return None), when there is no smaller number that contains the same digits. Also return -1 when the next smaller number with the same digits would require the leading digit to be zero.
     */


    /**
     * Given a list lst and a number N, create a new list that contains each number of lst at most N times without reordering.
     * For example if N = 2, and the input is [1,2,3,1,2,1,2,3], you take [1,2,3,1,2], drop the next [1,2] since this would lead to 1 and 2 being in the result 3 times,
     * and then take 3, which leads to [1,2,3,1,2,3].
     *
     * @param elements
     * @param maxOccurrences
     * @return
     */
    public static int[] deleteN(int[] elements, int maxOccurrences) {

        if(maxOccurrences == 0) {
            return new int[0];
        }

        Map<Integer, Integer> buffer = new HashMap<>();

        List<Integer> result = new ArrayList<>();

        for (int i = 0; i <= elements.length - 1; i++) {
            int element = elements[i];
            if (buffer.containsKey(element)) {
                Integer count = buffer.get(element);
                if (count < maxOccurrences) {
                    result.add(element);
                    count += 1;
                    buffer.put(element, count);
                }
            } else {
                buffer.put(element, 1);
                result.add(element);
            }
        }

        return result.stream().mapToInt(Integer::intValue).toArray();
    }


    /**
     * There is an array with some numbers. All numbers are equal except for one. Try to find it!
     *
     * Kata.findUniq(new double[]{ 1, 1, 1, 2, 1, 1 }); // => 2
     * Kata.findUniq(new double[]{ 0, 0, 0.55, 0, 0 }); // => 0.55
     * It’s guaranteed that array contains at least 3 numbers.
     *
     * The tests contain some very huge arrays, so think about performance.
     * @param arr
     * @return
     */
    public static double findUniq(double[] arr) {
        double[] reduced = Arrays.stream(arr).boxed().collect(Collectors.toSet()).stream().mapToDouble(Double::doubleValue).toArray();
        double result = reduced[0];
        int count = (int) Arrays.stream(arr).limit(3).filter(num -> num == result).count();


        return count < 2 ? result : reduced[1];
    }
}

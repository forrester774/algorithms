import java.util.Arrays;
import java.util.List;

public class MissingLetter {

    public static void main(String[] args) {
        System.out.println(findMissingLetter(new char[]{'a', 'b', 'c', 'd', 'f'})); // e
        System.out.println(findMissingLetter(new char[]{'O', 'Q', 'R', 'S'})); // P
    }

    /**
     * #Find the missing letter
     * <p>
     * Write a method that takes an array of consecutive (increasing) letters as input and that returns the missing letter in the array.
     * <p>
     * You will always get an valid array. And it will be always exactly one letter be missing. The length of the array will always be at least 2.
     * The array will always contain letters in only one case.
     * <p>
     * Example:
     * <p>
     * ['a','b','c','d','f'] -> 'e' ['O','Q','R','S'] -> 'P'
     * <p>
     * ["a","b","c","d","f"] -> "e"
     * ["O","Q","R","S"] -> "P"
     * (Use the English alphabet with 26 letters!)
     */

    public static char findMissingLetter(char[] array) {

        List<Character> alphabet = Arrays.asList('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

        char firstLetter = array[0];
        int indexOfFirstLetter = alphabet.indexOf(String.valueOf(firstLetter).toUpperCase().charAt(0));

        for (int i = 0; i < array.length; i++) {
            String currentCharacter = String.valueOf(array[i]);
            String checkedString = String.valueOf(alphabet.get(indexOfFirstLetter));
            if(!currentCharacter.equalsIgnoreCase(checkedString)) {
                return Character.isLowerCase(array[i])
                        ? checkedString.toLowerCase().charAt(0)
                        : checkedString.toUpperCase().charAt(0);
            }
            indexOfFirstLetter++;
        }

        return ' ';
    }
}

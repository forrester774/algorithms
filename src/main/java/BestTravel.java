import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class BestTravel {

    public static void main(String[] args) {
        System.out.println("****** Basic Tests small numbers******");
        List<Integer> ts = new ArrayList<>(Arrays.asList(50, 55, 56, 57, 58));
        int n = chooseBestSum(163, 2, ts);
        System.out.println(n); // 163
//        ts = new ArrayList<>(List.of(50));
//        Integer m = chooseBestSum(163, 3, ts);
//        System.out.println(m); // null
//        ts = new ArrayList<>(Arrays.asList(91, 74, 73, 85, 73, 81, 87));
//        n = chooseBestSum(230, 3, ts);
//        System.out.println(n); // 228
    }

    /**
     * John and Mary want to travel between a few towns A, B, C ... Mary has on a sheet of paper a list of distances
     * between these towns. ls = [50, 55, 57, 58, 60]. John is tired of driving and he says to Mary that he doesn't want
     * to drive more than t = 174 miles and he will visit only 3 towns.
     * <p>
     * Which distances, hence which towns, they will choose so that the sum of the distances is the biggest possible to please Mary and John?
     * <p>
     * Example:
     * With list ls and 3 towns to visit they can make a choice between: [50,55,57],[50,55,58],[50,55,60],[50,57,58],
     * [50,57,60],[50,58,60],[55,57,58],[55,57,60],[55,58,60],[57,58,60].
     * <p>
     * The sums of distances are then: 162, 163, 165, 165, 167, 168, 170, 172, 173, 175.
     * <p>
     * The biggest possible sum taking a limit of 174 into account is then 173 and the distances
     * of the 3 corresponding towns is [55, 58, 60].
     * <p>
     * The function chooseBestSum (or choose_best_sum or ... depending on the language) will take as
     * parameters t (maximum sum of distances, integer >= 0), k (number of towns to visit, k >= 1) and ls
     * (list of distances, all distances are positive or zero integers and this list has at least one element).
     * The function returns the "best" sum ie the biggest possible sum of k distances less than or equal to the given
     * limit t, if that sum exists, or otherwise nil, null, None, Nothing, depending on the language.
     * In that case with C, C++, D, Dart, Fortran, F#, Go, Julia, Kotlin, Nim, OCaml, Pascal, Perl, PowerShell, Reason, Rust, Scala, Shell, Swift return -1.
     * <p>
     * Examples:
     * ts = [50, 55, 56, 57, 58] choose_best_sum(163, 3, ts) -> 163
     * <p>
     * xs = [50] choose_best_sum(163, 3, xs) -> nil (or null or ... or -1 (C++, C, D, Rust, Swift, Go, ...)
     * <p>
     * ys = [91, 74, 73, 85, 73, 81, 87] choose_best_sum(230, 3, ys) -> 228
     * <p>
     * Notes:
     * try not to modify the input list of distances ls
     * in some languages this "list" is in fact a string (see the Sample Tests).
     */

    public static Integer chooseBestSum(int t, int k, List<Integer> ls) {
        if (k > ls.size()) {
            return null;
        }

        List<List<Integer>> resultSet = new ArrayList<>();

        for (int i = 0; i < ls.size() + 1 - k; i++) {
            for (int j = i + 1; j <= ls.size() + 1 - k; j++) {
                ArrayList<Integer> objects = new ArrayList<>();
                objects.add(ls.get(i));
                objects.add(ls.get(j));
                for (int l = j + 1; objects.size() < k; l++) {
                    objects.add(ls.get(l));
                }
                resultSet.add(objects);
            }
        }

        return resultSet
                .stream()
                .map(list -> list.stream().mapToInt(Integer::intValue)
                        .sum())
                .filter(num -> num <= t)
                .max(Comparator.comparingInt(Integer::intValue))
                .get();
    }
}

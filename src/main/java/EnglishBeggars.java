import java.util.*;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class EnglishBeggars {
    public static void main(String[] args) {
        int[] test = {1, 2, 3, 4, 5};
        int[] a1 = {15}, a2 = {9, 6}, a3 = {5, 7, 3}, a4 = {1, 2, 3, 4, 5, 0}, a5 = {};
        System.out.println(Arrays.toString(beggars(test, 1)));
        System.out.println(Arrays.toString(beggars(test, 2)));
        System.out.println(Arrays.toString(beggars(test, 3)));
        System.out.println(Arrays.toString(beggars(test, 6)));
        System.out.println(Arrays.toString(beggars(test, 0)));
    }

    /**
     *
     Born a misinterpretation of this kata, your task here is pretty simple:
     given an array of values and an amount of beggars, you are supposed to return an array with the sum of what each
     beggar brings home, assuming they all take regular turns, from the first to the last.

     For example: [1,2,3,4,5] for 2 beggars will return a result of [9,6], as the first one takes [1,3,5], the second collects [2,4].

     The same array with 3 beggars would have in turn have produced a better out come for the second beggar:
     [5,7,3], as they will respectively take [1,4], [2,5] and [3].

     Also note that not all beggars have to take the same amount of "offers", meaning that the length of the array
     is not necessarily a multiple of n; length can be even shorter, in which case the last beggars will of course take nothing (0).

     Note: in case you don't get why this kata is about English beggars, then you are not familiar
     on how religiously queues are taken in the kingdom ;)
     */

    public static int[] beggars(int[] values, int n) {

        Queue<Integer> queue = new LinkedList<>();

        Map<Integer, List<Integer>> map = new TreeMap<>();
        for (int i = 0; i < n; i++) {
            queue.add(i);
            ArrayList<Integer> list = new ArrayList<>();
            list.add(0);
            map.put(i, list);
        }



        for (int num : values) {
            Integer beggar = queue.poll();
            map.computeIfAbsent(beggar, key -> new ArrayList<>()).add(num);
            queue.add(beggar);
        }

        return map
                .values()
                .stream()
                .map(list -> list.stream().mapToInt(Integer::intValue).sum())
                .mapToInt(Integer::intValue)
                .toArray();

    }
}

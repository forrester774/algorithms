import java.util.Objects;

public class ShiftedDif {

    public static void main(String[] args) {
        System.out.println(shiftedDiff("hoop", "pooh")); // -1
        System.out.println(shiftedDiff("coffee", "eecoff")); // 2
        System.out.println(shiftedDiff("eecoff", "coffee"));  // 4
    }

    /**
     * Write a function that receives two strings and returns n, where n is equal to the number
     * of characters we should shift the first string forward to match the second. The check should be case sensitive.
     * <p>
     * For instance, take the strings "fatigue" and "tiguefa". In this case, the first string
     * has been rotated 5 characters forward to produce the second string, so 5 would be returned.
     * <p>
     * If the second string isn't a valid rotation of the first string, the method returns -1.
     * Examples:
     * "coffee", "eecoff" => 2
     * "eecoff", "coffee" => 4
     * "moose", "Moose" => -1
     * "isn't", "'tisn" => 2
     * "Esham", "Esham" => 0
     * "dog", "god" => -1
     */

    static int shiftedDiff(String first, String second) {
        if (Objects.isNull(first) || Objects.isNull(second)) {
            return 0;
        }

        int count = 0;

        while (!first.equals(second) && count < first.length()) {
            first = first.charAt(first.length() - 1) + first.substring(0, first.length() - 1);
            count++;
            if (first.equals(second)) {
                return count;
            }
        }
        return -1;
    }
}

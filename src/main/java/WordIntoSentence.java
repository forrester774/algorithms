import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class WordIntoSentence {
    public static void main(String[] args) {
        System.out.println(formatWords(new String[]{"one", "two", "three", "four"}));
        System.out.println(formatWords(new String[]{"one"}));
        System.out.println(formatWords(new String[]{"one", "", "three"}));
        System.out.println(formatWords(new String[]{"", "", "three"}));
        System.out.println(formatWords(new String[]{"one", "two", ""}));
        System.out.println(formatWords(new String[]{}));
        System.out.println(formatWords(null));
        System.out.println(formatWords(new String[]{""}));
    }

    /**
     * Complete the method so that it formats the words into a single comma separated value.
     * The last word should be separated by the word 'and' instead of a comma. The method takes in an array of strings and returns a single formatted string.
     * <p>
     * Note:
     * <p>
     * Empty string values should be ignored.
     * Empty arrays or null/nil/None values being passed into the method should result in an empty string being returned.
     * Example: (Input --> output)
     * <p>
     * ['ninja', 'samurai', 'ronin'] --> "ninja, samurai and ronin"
     * ['ninja', '', 'ronin'] --> "ninja and ronin"
     * [] -->""
     */

    public static String formatWords(String[] words) {

        if (Objects.isNull(words) || words.length == 0) {
            return "";
        }

        List<String> nonEmptyWords = Arrays.stream(words).filter(word -> !word.isBlank()).collect(Collectors.toList());

        if (nonEmptyWords.isEmpty()) {
            return "";
        }

        String join = String.join(", ", nonEmptyWords.subList(0, nonEmptyWords.size() - 1));
        return nonEmptyWords.size() >= 2 ? join + " and " + nonEmptyWords.get(nonEmptyWords.size() - 1) : nonEmptyWords.get(nonEmptyWords.size() - 1);
    }
}

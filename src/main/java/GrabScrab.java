import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class GrabScrab {

    public static void main(String[] args) {

    }

    /**
     * Pirates have notorious difficulty with enunciating. They tend to blur all the letters together and scream at people.
     * <p>
     * At long last, we need a way to unscramble what these pirates are saying.
     * <p>
     * Write a function that will accept a jumble of letters as well as a dictionary, and output a list of words that the pirate might have meant.
     * <p>
     * For example:
     * <p>
     * grabscrab( "ortsp", ["sport", "parrot", "ports", "matey"] )
     * Should return ["sport", "ports"].
     * <p>
     * Return matches in the same order as in the dictionary. Return an empty array if there are no matches.
     */

    public static List<String> grabscrab(String s, List<String> words) {

        if (Objects.isNull(s) || s.isBlank() || Objects.isNull(words) || words.isEmpty()) {
            return new ArrayList<>();
        }

        List<String> result = new ArrayList<>();
        String sortedInput = Arrays.stream(s.split(""))
                .sorted()
                .collect(Collectors.joining());

        for (String word : words) {
            String sortedWord = Arrays.stream(word.split(""))
                    .sorted()
                    .collect(Collectors.joining());
            if (sortedWord.equals(sortedInput)) {
                result.add(word);
            }
        }
        return result;
    }
}

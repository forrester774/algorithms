public class Factorial {
    public static void main(String[] args) {
        System.out.println(factorial(5));

        System.out.println(factorialLoop(5));
    }

    public static int factorial(int fact) {
        if (fact <= 1) {
            return 1;
        }
        return fact * factorial(fact - 1);
    }

    public static int factorialLoop(int fact) {
        int product = 1;
        for (int i = 2; i <= fact; i++) {
            product *= i;
        }

        return product;
    }
}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MexicanWave {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(wave(" Gap ")));
    }

    public static String[] wave(String str) {
        List<String> resultSet = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(str.toLowerCase());
            char character = str.charAt(i);

            if (String.valueOf(character).isBlank()) {
                continue;
            }

            stringBuilder.setCharAt(i, Character.toUpperCase(character));
            resultSet.add(stringBuilder.toString());
        }
        return resultSet.toArray(new String[0]);
    }
    
}
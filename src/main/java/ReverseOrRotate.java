import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ReverseOrRotate {
    public static void main(String[] args) {
        System.out.println("Fixed Tests: revRot");
        System.out.println(revRot("1234", 0)); // , ""
        System.out.println(revRot("", 0)); // , ""
        System.out.println(revRot("1234", 5)); // , ""
        String s = "733049910872815764";
        System.out.println(revRot(s, 5)); // , "330479108928157"

        System.out.println(Character.getNumericValue('5'));
    }

    /**
     * The input is a string str of digits. Cut the string into chunks
     * (a chunk here is a substring of the initial string) of size sz (ignore the last chunk if its size is less than sz).
     * <p>
     * If a chunk represents an integer such as the sum of the cubes of its digits is divisible by 2,
     * reverse that chunk; otherwise rotate it to the left by one position. Put together these modified chunks and return the result as a string.
     * <p>
     * If
     * <p>
     * sz is <= 0 or if str is empty return ""
     * sz is greater (>) than the length of str it is impossible to take a chunk of size sz hence return "".
     * Examples:
     * revrot("123456987654", 6) --> "234561876549"
     * revrot("123456987653", 6) --> "234561356789"
     * revrot("66443875", 4) --> "44668753"
     * revrot("66443875", 8) --> "64438756"
     * revrot("664438769", 8) --> "67834466"
     * revrot("123456779", 8) --> "23456771"
     * revrot("", 8) --> ""
     * revrot("123456779", 0) --> ""
     * revrot("563000655734469485", 4) --> "0365065073456944"
     * Example of a string rotated to the left by one position:
     * s = "123456" gives "234561".
     */

    public static String revRot(String strng, int sz) {
        if (sz == 0 || Objects.isNull(strng) || strng.isBlank() || strng.length() < sz) {
            return "";
        }

        List<String> collect = Arrays
                .stream(strng.split("(?<=\\G.{" + sz + "})"))
                .filter(nums -> nums.length() == sz)
                .collect(Collectors.toList());

        StringBuilder builder = new StringBuilder();

        for (String nums : collect) {
            List<Integer> ints = Arrays.stream(nums.split(""))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
            int sum = 0;
            for (int num : ints) {
                sum += num * num * num;
            }

            if (sum % 2 == 0) {
                StringBuilder tempBuilder = new StringBuilder(nums);
                builder.append(tempBuilder.reverse());
            } else {
                builder.append(nums.substring(1))
                        .append(nums.charAt(0));
            }
        }

        return builder.toString();
    }
}

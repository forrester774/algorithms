import java.util.Arrays;

public class MergeSort {

    public static void main(String[] args) {
        int[] ints = {10, 7, 2, 4, 3, 8, 9, 1, 5};

        System.out.println(Arrays.toString(mergeSort(ints)));
    }

    public static int[] mergeSort(int[] ints) {
        if (ints.length <= 1) {
            return ints;
        }

        int mid = ints.length / 2;
        int[] left = new int[mid];
        int[] right = new int[ints.length - mid];

        int rightCount = 0;

        for (int i = 0; i <= ints.length - 1; i++) {
            if (i < mid) {
                left[i] = ints[i];
            } else {
                right[rightCount] = ints[i];
                rightCount++;
            }
        }

        mergeSort(left);
        mergeSort(right);

        combine(ints, left, right);

        return ints;
    }

    public static void combine(int[] ints, int[] left, int[] right) {

        int leftLength = left.length;
        int rightLength = right.length;

        int leftCount = 0;
        int rightCount = 0;
        int merged = 0;

        while (leftCount < leftLength && rightCount < rightLength) {
            if (left[leftCount] <= right[rightCount]) {
                ints[leftCount + rightCount] = left[leftCount];
                leftCount++;
            } else {
                ints[leftCount + rightCount] = right[rightCount];
                rightCount++;
            }
            merged++;
        }

        System.arraycopy(left, leftCount, ints, merged, left.length - leftCount);
        System.arraycopy(right, rightCount, ints, merged, right.length - rightCount);

    }
}

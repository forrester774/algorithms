import java.util.Objects;

public class StringIncrementer {
    public static void main(String[] args) {
        System.out.println(incrementString(""));
    }

    /**
     * Your job is to write a function which increments a string, to create a new string.
     * <p>
     * If the string already ends with a number, the number should be incremented by 1.
     * If the string does not end with a number. the number 1 should be appended to the new string.
     * Examples:
     * <p>
     * foo -> foo1
     * <p>
     * foobar23 -> foobar24
     * <p>
     * foo0042 -> foo0043
     * <p>
     * foo9 -> foo10
     * <p>
     * foo099 -> foo100
     * <p>
     * Attention: If the number has leading zeros the amount of digits should be considered.
     */

    public static String incrementString(String str) {
        if (Objects.isNull(str) || str.isBlank()) {
            return "1";
        }

        String[] split = str.split("(?=[0-9]+)");

        if (split.length == 1) {
            return isNumeric(str) ? Long.valueOf(str) + 1 + "" :incrementer(split);
        } else {
            return split[0] + incrementer(split);
        }
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static String incrementer(String[] split) {
        StringBuilder allNums = new StringBuilder();

        for (int i = 1; i < split.length; i++) {
            allNums.append(split[i]);
        }

        long incremented = Long.valueOf(allNums.toString()) + 1;
        StringBuilder finalString = new StringBuilder(String.valueOf(incremented));
        while (finalString.length() < allNums.length()) {
            finalString.insert(0, "0");
        }

        return finalString.toString();
    }
}

import java.util.Arrays;
import java.util.stream.Collectors;

public class PhoneNumber {

    public static void main(String[] args) {
        System.out.println(createPhoneNumber(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 0})); // (123) 456-7890
    }

    /**
     * Write a function that accepts an array of 10 integers (between 0 and 9), that returns a string of those numbers in the form of a phone number.
     *
     * Example
     * Kata.createPhoneNumber(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 0}) // => returns "(123) 456-7890"
     * The returned format must be correct in order to complete this challenge.
     *
     * Don't forget the space after the closing parentheses!
     */

    public static String createPhoneNumber(int[] numbers) {

        StringBuilder builder = new StringBuilder("(");

       String numbersList = Arrays
               .stream(numbers)
               .boxed().map(String::valueOf)
               .collect(Collectors.joining());

        builder.append(numbersList, 0, 3)
                .append(") ")
                .append(numbersList, 3, 6)
                .append("-")
                .append(numbersList, 6, 10);

        return builder.toString();
    }
}

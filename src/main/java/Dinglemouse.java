public class Dinglemouse {

    public static void main(String[] args) {
        //System.out.println(int123(500));
        final long b = Dinglemouse.int123(500);
        System.out.println("" + 500 + " + " + b + " = " + (int) (500 + b));
    }


    /**
     * Given number A you must return number B so that
     * <p>
     * (int) (A + B) == 123
     * <p>
     * Note
     * <p>
     * B can't be negative
     *
     * @param a
     * @return
     */
    public static long int123(final int a) {
        int result = 123 - a;
        int abs = Math.abs(result);

        if (result >= 0) {
            return abs;
        } else {
            return Math.abs(Integer.MAX_VALUE*2+2 - (a - 123));
        }
    }
}
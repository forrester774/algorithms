public class PagesInABook {
    public static void main(String[] args) {

                System.out.println(amountOfPages(5)); // 5
                System.out.println(amountOfPages(25)); // 17
                System.out.println(amountOfPages(1095)); // 401
                System.out.println(amountOfPages(185)); // 97
                System.out.println(amountOfPages(660)); // 256
            }

    /**
     * Every book has n pages with page numbers 1 to n. The summary is made by adding up the number of digits of all page numbers.
     * <p>
     * Task: Given the summary, find the number of pages n the book has.
     * <p>
     * Example
     * If the input is summary=25, then the output must be n=17: The numbers 1 to 17 have 25 digits in total: 1234567891011121314151617.
     * <p>
     * Be aware that you'll get enormous books having up to 100.000 pages.
     * <p>
     * All inputs will be valid.
     */
    public static int amountOfPages(int summary) {

        int numberOfPages = 0;
        StringBuilder concatedPageNumbers = new StringBuilder();
        while(concatedPageNumbers.length() < summary) {
            numberOfPages++;
            concatedPageNumbers.append(numberOfPages);
        }

        return numberOfPages;
    }
}

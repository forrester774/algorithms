import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class RomanNumerals {

    private static TreeMap<Integer, String> MAP;
    static {
        MAP = new TreeMap<Integer, String>(Collections.reverseOrder());
        MAP.put( 1, "I" );
        MAP.put( 4, "IV" );
        MAP.put( 5, "V" );
        MAP.put( 9, "IX" );
        MAP.put( 10, "X" );
        MAP.put( 40, "XL" );
        MAP.put( 50, "L" );
        MAP.put( 90, "XC" );
        MAP.put( 100, "C" );
        MAP.put( 400, "CD" );
        MAP.put( 500, "D" );
        MAP.put( 900, "CM" );
        MAP.put( 1000, "M" );
    }

    public static String solution(int n) {
        StringBuilder builder = new StringBuilder();
        for( Map.Entry<Integer, String> entry: MAP.entrySet() ){
            int i = entry.getKey();
            String s = entry.getValue();
            while( n>=i ){
                builder.append(s);
                n -= i;
            }
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        System.out.println(solution(1990)); // "solution(1) should equal to I", "I",
        System.out.println(solution(4)); // "solution(4) should equal to IV", "IV",
        System.out.println(solution(6)); // "solution(6) should equal to VI", "VI",
    }

    /**
     * Create a function taking a positive integer as its parameter and returning a string containing the Roman Numeral representation of that integer.
     *
     * Modern Roman numerals are written by expressing each digit separately starting with the left most digit and
     * skipping any digit with a value of zero. In Roman numerals 1990 is rendered: 1000=M, 900=CM, 90=XC; resulting
     * in MCMXC. 2008 is written as 2000=MM, 8=VIII; or MMVIII. 1666 uses each Roman symbol in descending order: MDCLXVI.
     *
     * Example:
     *
     * conversion.solution(1000); //should return "M"
     * Help:
     *
     * Symbol    Value
     * I          1
     * V          5
     * X          10
     * L          50
     * C          100
     * D          500
     * M          1,000
     * Remember that there can't be more than 3 identical symbols in a row.
     */

}

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Pangram {

    public static void main(String[] args) {

    }

    /**
     * A pangram is a sentence that contains every single letter of the alphabet
     * at least once. For example, the sentence "The quick brown fox jumps over the lazy dog"
     * is a pangram, because it uses the letters A-Z at least once (case is irrelevant).
     *
     * Given a string, detect whether or not it is a pangram. Return True if it is, False if not. Ignore numbers and punctuation.
     */



    public boolean check(String sentence){
        if(Objects.isNull(sentence) || sentence.isBlank()) {
            return false;
        }

        List<String> alphabet = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
                "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        return new HashSet<>(Arrays.stream(sentence.split("")).map(String::toLowerCase).collect(Collectors.toList())).containsAll(alphabet);
    }
}

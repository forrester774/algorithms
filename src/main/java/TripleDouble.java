public class TripleDouble {

    public static void main(String[] args) {

        System.out.println(TripleDouble(451999277L, 41177722899L)); // 1
        System.out.println(TripleDouble(1222345L, 12345L)); // 0
        System.out.println(TripleDouble(12345L, 12345L)); // 0
        System.out.println(TripleDouble(666789L, 12345667L)); // 1
        System.out.println(TripleDouble(451999277L, 411777228L)); //0
        System.out.println(TripleDouble(1112L, 122L)); // 0

    }

    /**
     * Write a function
     * <p>
     * TripleDouble(long num1, long num2)
     * which takes numbers num1 and num2 and returns 1 if there is a straight triple of a
     * number at any place in num1 and also a straight double of the same number in num2.
     * <p>
     * If this isn't the case, return 0
     * <p>
     * Examples
     * TripleDouble(451999277, 41177722899) == 1 // num1 has straight triple 999s and
     * // num2 has straight double 99s
     * <p>
     * TripleDouble(1222345, 12345) == 0 // num1 has straight triple 2s but num2 has only a single 2
     * <p>
     * TripleDouble(12345, 12345) == 0
     * <p>
     * TripleDouble(666789, 12345667) == 1
     */

    public static int TripleDouble(long num1, long num2) {

        String num1String = String.valueOf(num1);
        String num2String = String.valueOf(num2);

        String[] split = num1String.split("");

        for (int i = 0; i < split.length - 2; i++) {
            String first = split[i];
            String second = split[i+1];
            String third = split[i+2];

            if(first.equals(second) && first.equals(third) && num2String.contains(first+second)) {
                return 1;
            }
        }
        return 0;
    }
}

import java.util.*;
import java.util.stream.Collectors;

public class Meeting {
    public static void main(String[] args) {
        test();
    }
    public static void test() {
        testing("Sophia:Korn;Jacob:Bell;Sydney:Wahl;Amandy:Meta;Amandy:Korn;Ann:Kern;Sophia:Korn;Emily:Bell;Lewis:Bell;Anna:Meta;Ann:Rudd;Alissa:Wahl;Sarah:Arno;Sydney:Wahl;Jacob:Cornwell;Elizabeth:Meta;Ann:Kern;Grace:Gates;Amber:Gate",
                "(ARNO, SARAH)(BELL, EMILY)(BELL, JACOB)(BELL, LEWIS)(CORNWELL, JACOB)(GATE, AMBER)(GATES, GRACE)(KERN, ANN)(KERN, ANN)(KORN, AMANDY)(KORN, SOPHIA)(KORN, SOPHIA)(META, AMANDY)(META, ANNA)(META, ELIZABETH)(RUDD, ANN)(WAHL, ALISSA)(WAHL, SYDNEY)(WAHL, SYDNEY)");
//        testing("John:Gates;Michael:Wahl;Megan:Bell;Paul:Dorries;James:Dorny;Lewis:Steve;Alex:Meta;Elizabeth:Russel;Anna:Korn;Ann:Kern;Amber:Cornwell",
//                "(BELL, MEGAN)(CORNWELL, AMBER)(DORNY, JAMES)(DORRIES, PAUL)(GATES, JOHN)(KERN, ANN)(KORN, ANNA)(META, ALEX)(RUSSEL, ELIZABETH)(STEVE, LEWIS)(WAHL, MICHAEL)");
//        testing("Alex:Arno;Alissa:Cornwell;Sarah:Bell;Andrew:Dorries;Ann:Kern;Haley:Arno;Paul:Dorny;Madison:Kern",
//                "(ARNO, ALEX)(ARNO, HALEY)(BELL, SARAH)(CORNWELL, ALISSA)(DORNY, PAUL)(DORRIES, ANDREW)(KERN, ANN)(KERN, MADISON)");

    }


    private static void testing(String s, String exp) {
        System.out.println("Testing:\n" + s);
        String ans = Meeting.meeting(s);
        System.out.println("Answer: " + ans);
        System.out.println("Expect: " + exp);
    }

    /**
     * John has invited some friends. His list is:
     *
     * s = "Fred:Corwill;Wilfred:Corwill;Barney:Tornbull;Betty:Tornbull;Bjon:Tornbull;Raphael:Corwill;Alfred:Corwill";
     * Could you make a program that
     *
     * makes this string uppercase
     * gives it sorted in alphabetical order by last name.
     * When the last names are the same, sort them by first name. Last name and first name of a guest come in the result between parentheses separated by a comma.
     *
     * So the result of function meeting(s) will be:
     *
     * "(CORWILL, ALFRED)(CORWILL, FRED)(CORWILL, RAPHAEL)(CORWILL, WILFRED)(TORNBULL, BARNEY)(TORNBULL, BETTY)(TORNBULL, BJON)"
     * It can happen that in two distinct families with the same family name two people have the same first name too.
     */

    public static String meeting(String s) {
        String[] splitBySemicolon = s.split(";");

        Map<String, ArrayList<String>> result = new TreeMap<>();

        for (String name: splitBySemicolon) {
            String[] namesSplit = name.split(":");

            result.computeIfAbsent(namesSplit[1].toUpperCase(), key -> new ArrayList<>()).add(namesSplit[0].toUpperCase());
        }


        return result.entrySet().stream().map(entry -> {
            StringBuilder builder = new StringBuilder();
            for (String name : entry.getValue().stream().sorted().collect(Collectors.toList())) {
                builder.append("(")
                        .append(entry.getKey())
                        .append(", ")
                        .append(name)
                        .append(")");
            }
            return builder.toString();
        }).collect(Collectors.joining());
    }
}

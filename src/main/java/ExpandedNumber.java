import java.util.ArrayList;
import java.util.List;

public class ExpandedNumber {

    public static void main(String[] args) {
        System.out.println(expandedForm(12)); // 10 + 2
        System.out.println(expandedForm(1)); // 1
        System.out.println(expandedForm(42)); // 40 + 2
        System.out.println(expandedForm(70304)); // 70000 + 300 + 4
    }

    /**
     * Write Number in Expanded Form
     * You will be given a number and you will need to return it as a string in Expanded Form. For example:
     * <p>
     * Kata.expandedForm(12); # Should return "10 + 2"
     * Kata.expandedForm(42); # Should return "40 + 2"
     * Kata.expandedForm(70304); # Should return "70000 + 300 + 4"
     * NOTE: All numbers will be whole numbers greater than 0.
     */

    public static String expandedForm(int num) {

        String[] digits = String.valueOf(num).split("");

        List<String> result = new ArrayList<>();

        for (int i = 0; i < digits.length; i++) {
            String digit = digits[i];
            StringBuilder builder = new StringBuilder(digit);

            if (digit.equals("0")) {
                continue;
            }

            for (int j = i; j < digits.length - 1; j++) {
                builder.append("0");
            }
            result.add(builder.toString());
        }

        return String.join(" + ", result);
    }
}

import java.util.Objects;

public class ValidParenthesis {

    public static void main(String[] args) {

        System.out.println(validParentheses("()()((()")); // false
        System.out.println(validParentheses("()")); // true
        System.out.println(validParentheses("()()")); // true
        System.out.println(validParentheses("(())")); // true
        System.out.println(validParentheses(")")); // true
    }

    /**
     * Write a function that takes a string of parentheses, and determines if the order of the parentheses is valid.
     * The function should return true if the string is valid, and false if it's invalid.
     * <p>
     * Examples
     * "()"              =>  true
     * ")(()))"          =>  false
     * "("               =>  false
     * "(())((()())())"  =>  true
     * Constraints
     * 0 <= input.length <= 100
     * <p>
     * Along with opening (() and closing ()) parenthesis, input may contain any valid ASCII characters.
     * Furthermore, the input string may be empty and/or not contain any parentheses at all.
     * Do not treat other forms of brackets as parentheses (e.g. [], {}, <>).
     */

    public static boolean validParentheses(String parens) {
        if (Objects.isNull(parens) || parens.isBlank()) {
            return false;
        }

        String[] split = parens.replace(" ", "").split("");
        int counter = 0;
        for (int i = 0; i < split.length; i++) {
            String current = split[i];
            if (current.equals(")") && counter == 0) {
                return false;
            }

            if (current.equals("(")) {
                counter++;
            } else if (current.equals(")")) {
                counter--;
            }
        }

        return counter == 0;
    }
}

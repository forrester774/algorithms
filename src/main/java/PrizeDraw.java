import java.util.*;
import java.util.stream.Collectors;

public class PrizeDraw {

    public static void main(String[] args) {
        String st = "Elijah,Chloe,Elizabeth,Matthew,Natalie,Jayden";
        Integer[] we = new Integer[] {1, 3, 5, 5, 3, 6};
        System.out.println(nthRank(st, we, 2)); // Matthew
    }

    /**
     * To participate in a prize draw each one gives his/her firstname.
     * <p>
     * Each letter of a firstname has a value which is its rank in the English alphabet. A and a have rank 1, B and b rank 2 and so on.
     * <p>
     * The length of the firstname is added to the sum of these ranks hence a number som.
     * <p>
     * An array of random weights is linked to the firstnames and each som is multiplied by its corresponding weight to get what they call a winning number.
     * <p>
     * Example:
     * names: "COLIN,AMANDBA,AMANDAB,CAROL,PauL,JOSEPH"
     * weights: [1, 4, 4, 5, 2, 1]
     * <p>
     * PauL -> som = length of firstname + 16 + 1 + 21 + 12 = 4 + 50 -> 54
     * The *weight* associated with PauL is 2 so PauL's *winning number* is 54 * 2 = 108.
     * Now one can sort the firstnames in decreasing order of the winning numbers.
     * When two people have the same winning number sort them alphabetically by their firstnames.
     * <p>
     * Task:
     * parameters: st a string of firstnames, we an array of weights, n a rank
     * <p>
     * return: the firstname of the participant whose rank is n (ranks are numbered from 1)
     * <p>
     * Example:
     * names: "COLIN,AMANDBA,AMANDAB,CAROL,PauL,JOSEPH"
     * weights: [1, 4, 4, 5, 2, 1]
     * n: 4
     * <p>
     * The function should return: "PauL"
     * Notes:
     * The weight array is at least as long as the number of names, it may be longer.
     * <p>
     * If st is empty return "No participants".
     * <p>
     * If n is greater than the number of participants then return "Not enough participants".
     */

    public static String nthRank(String st, Integer[] we, int n) {
        if (Objects.isNull(st) || st.isBlank()) {
            return "No participants";
        }

        if (n > we.length) {
            return "Not enough participants";
        }

        List<Character> alphabet = List.of('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
        String[] names = st.split(",");

        Map<Integer, List<String>> resultSet = new HashMap<>();

        for (int i = 0; i < names.length; i++) {
            String name = names[i];
            int sum = name.length();
            for (int j = 0; j < name.length(); j++) {
                sum += alphabet.indexOf(name.toLowerCase().charAt(j)) + 1;
            }
            sum *= we[i];
            resultSet.computeIfAbsent(sum, key -> new ArrayList<>()).add(name);
        }
        List<String> result = new ArrayList<>();
        resultSet.keySet().stream().sorted(Comparator.reverseOrder()).forEach(key -> {
            resultSet.get(key).stream().sorted().forEach(result::add);
        });
        return result.get(n-1);
    }
}

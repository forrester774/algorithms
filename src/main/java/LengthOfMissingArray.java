import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class LengthOfMissingArray {
    public static void main(String[] args) {
        System.out.println(getLengthOfMissingArray(new Object[][]{new Object[]{1, 2}, new Object[]{4, 5, 1, 1}, new Object[]{1}, new Object[]{5, 6, 7, 8, 9}}));
        // 3
        System.out.println(getLengthOfMissingArray(new Object[][]{new Object[]{5, 2, 9}, new Object[]{4, 5, 1, 1}, new Object[]{1}, new Object[]{5, 6, 7, 8, 9}}));
        // 2
        System.out.println(getLengthOfMissingArray(new Object[][]{new Object[]{null}, new Object[]{null, null, null}}));
        // 2
        System.out.println(getLengthOfMissingArray(new Object[][]{new Object[]{'a', 'a', 'a'}, new Object[]{'a', 'a'}, new Object[]{'a', 'a', 'a', 'a'}, new Object[]{'a'}, new Object[]{'a', 'a', 'a', 'a', 'a', 'a'}}));
        // 5
        System.out.println(getLengthOfMissingArray(new Object[][]{}));
        // 0
    }

    /**
     * You get an array of arrays.
     * If you sort the arrays by their length, you will see, that their length-values are consecutive.
     * But one array is missing!
     * <p>
     * <p>
     * You have to write a method, that return the length of the missing array.
     * <p>
     * Example:
     * [[1, 2], [4, 5, 1, 1], [1], [5, 6, 7, 8, 9]] --> 3
     * <p>
     * If the array of arrays is null/nil or empty, the method should return 0.
     * <p>
     * When an array in the array is null or empty, the method should return 0 too!
     * There will always be a missing element and its length will be always between the given arrays.
     */

    public static int getLengthOfMissingArray(Object[][] arrayOfArrays) {
        if (Objects.isNull(arrayOfArrays) || arrayOfArrays.length == 0) {
            return 0;
        }

        Set<Integer> set = new TreeSet<>();

        for (Object[] array : arrayOfArrays) {
            if(Objects.isNull(array) || array.length == 0) {
                return 0;
            }
            set.add(array.length);
        }

        int[] setArray = set.stream().mapToInt(Integer::intValue).toArray();

        for (int i = 0; i < set.size() - 1; i++) {
            if (setArray[i] + 1 != setArray[i + 1]) {
                return setArray[i] + 1;
            }
        }
        return 0;
    }
}

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class ConsecutiveStrings {

    public static void main(String[] args) {
        test();
    }

    private static void testing(String actual, String expected) {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("Actual: " + actual);
        System.out.println("---------");
        System.out.println("Expected: " + expected);
        System.out.println("-------------------------------------------------------------------------------");
    }

    public static void test() {
        testing(longestConsec(new String[]{"zone", "abigail", "theta", "form", "libe", "zas", "theta", "abigail"}, 2), "abigailtheta");
        testing(longestConsec(new String[]{"ejjjjmmtthh", "zxxuueeg", "aanlljrrrxx", "dqqqaaabbb", "oocccffuucccjjjkkkjyyyeehh"}, 1), "oocccffuucccjjjkkkjyyyeehh");
        testing(longestConsec(new String[]{}, 3), "");
        testing(longestConsec(new String[]{"itvayloxrp", "wkppqsztdkmvcuwvereiupccauycnjutlv", "vweqilsfytihvrzlaodfixoyxvyuyvgpck"}, 2), "wkppqsztdkmvcuwvereiupccauycnjutlvvweqilsfytihvrzlaodfixoyxvyuyvgpck");
        testing(longestConsec(new String[]{"wlwsasphmxx", "owiaxujylentrklctozmymu", "wpgozvxxiu"}, 2), "wlwsasphmxxowiaxujylentrklctozmymu");
        testing(longestConsec(new String[]{"zone", "abigail", "theta", "form", "libe", "zas"}, -2), "");
        testing(longestConsec(new String[]{"it", "wkppv", "ixoyx", "3452", "zzzzzzzzzzzz"}, 3), "ixoyx3452zzzzzzzzzzzz");
        testing(longestConsec(new String[]{"it", "wkppv", "ixoyx", "3452", "zzzzzzzzzzzz"}, 15), "");
        testing(longestConsec(new String[]{"it", "wkppv", "ixoyx", "3452", "zzzzzzzzzzzz"}, 0), "");
    }

    /**
     * You are given an array(list) strarr of strings and an integer k. Your task is to return the first longest string consisting of
     * k consecutive strings taken in the array.
     * <p>
     * Examples:
     * strarr = ["tree", "foling", "trashy", "blue", "abcdef", "uvwxyz"], k = 2
     * <p>
     * Concatenate the consecutive strings of strarr by 2, we get:
     * <p>
     * treefoling   (length 10)  concatenation of strarr[0] and strarr[1]
     * folingtrashy ("      12)  concatenation of strarr[1] and strarr[2]
     * trashyblue   ("      10)  concatenation of strarr[2] and strarr[3]
     * blueabcdef   ("      10)  concatenation of strarr[3] and strarr[4]
     * abcdefuvwxyz ("      12)  concatenation of strarr[4] and strarr[5]
     * <p>
     * Two strings are the longest: "folingtrashy" and "abcdefuvwxyz".
     * The first that came is "folingtrashy" so
     * longest_consec(strarr, 2) should return "folingtrashy".
     * <p>
     * In the same way:
     * longest_consec(["zone", "abigail", "theta", "form", "libe", "zas", "theta", "abigail"], 2) --> "abigailtheta"
     * n being the length of the string array, if n = 0 or k > n or k <= 0 return "" (return Nothing in Elm, "nothing" in Erlang).
     * <p>
     * Note
     * consecutive strings : follow one after another without an interruption
     */

    public static String longestConsec(String[] strarr, int k) {
        if (Objects.isNull(strarr) || strarr.length == 0 || k == 0 || k > strarr.length) {
            return "";
        }

        List<String> combinations = new ArrayList<>();

        for (int i = 0; i <= strarr.length - k; i++) {
            StringBuilder builder = new StringBuilder();
            for (int j = i; j < i + k; j++) {
                builder.append(strarr[j]);
            }
            combinations.add(builder.toString());
        }

        return combinations.stream().max(Comparator.comparing(String::length)).get();
    }


}

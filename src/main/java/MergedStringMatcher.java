import java.util.Objects;

public class MergedStringMatcher {
    public static void main(String[] args) {
        System.out.println(isMerge("a0Q^HrDw!U'(_[7Y-/A?hc!;u5$v$[/0e@ua0Q^HrDw!U'(_[7Y-/A?hc!;u6y8b-an`:^`;gLXKUdiNIzhQX=4D7m 86Q\\gtwCvQ;WOE,q&Z7", "a0Q^HrDw!U'(_[7Y-/A?hc!;u6y8b-an`:^`;gLXKUdiNIzh", "a0Q^HrDw!U'(_[7Y-/A?hc!;u5$v$[/0e@uQX=4D7m 86Q\\gtwCvQ;WOE,q&Z7"));
    }

    /**
     * At a job interview, you are challenged to write an algorithm to check if a given string, s, can be formed from two other strings, part1 and part2.
     * <p>
     * The restriction is that the characters in part1 and part2 should be in the same order as in s.
     * <p>
     * The interviewer gives you the following example and tells you to figure out the rest from the given test cases.
     * <p>
     * For example:
     * <p>
     * 'codewars' is a merge from 'cdw' and 'oears':
     * <p>
     * s:  c o d e w a r s   = codewars
     * part1:  c   d   w         = cdw
     * part2:    o   e   a r s   = oears
     */

    public static boolean isMerge(String s, String part1, String part2) {
        if (Objects.isNull(s) || Objects.isNull(part1) || Objects.isNull(part2)) {
            return false;
        }

        String[] split = s.split("");
        String[] split1 = part1.split("");
        String[] split2 = part2.split("");
        int part1Counter = 1;
        int part2Counter = 1;
        StringBuilder builder = new StringBuilder();
        for (String character : split) {
            if (character.equals(split1[0])) {
                split1 = part1.substring(part1Counter).split("");
                part1Counter++;
                builder.append(character);

            } else if (character.equals(split2[0])) {
                split2 = part2.substring(part2Counter).split("");
                part2Counter++;
                builder.append(character);
            } else {
                return false;
            }
        }
        return builder.toString().equals(s);
    }
}

import java.util.Arrays;
import java.util.stream.Collectors;

public class BreakCamelCase {
    public static void main(String[] args) {
        System.out.println(camelCase("camelCasing"));
        System.out.println(camelCase("camelCasingTest"));
        System.out.println(camelCase("camelcasingtest"));
    }

    /**
     * Complete the solution so that the function will break up camel casing, using a space between words.
     *
     * Example
     * "camelCasing"  =>  "camel Casing"
     * "identifier"   =>  "identifier"
     * ""             =>  ""
     */

    public static String camelCase(String input) {
        return String.join(" ", input.split("(?=[A-Z])"));
    }
}

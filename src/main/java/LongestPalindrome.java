import java.util.Objects;

public class LongestPalindrome {

    public static void main(String[] args) {
        basicTests();
    }

    /**
     * Longest Palindrome
     * Find the length of the longest substring in the given string s that is the same in reverse.
     * <p>
     * As an example, if the input was “I like racecars that go fast”, the substring (racecar) length would be 7.
     * <p>
     * If the length of the input string is 0, the return value must be 0.
     * <p>
     * Example:
     * "a" -> 1
     * "aab" -> 2
     * "abcde" -> 1
     * "zzbaabcd" -> 4
     * "" -> 0
     */

    public static void basicTests() {
        doTest("a", 1);
        doTest("aa", 2);
        doTest("baa", 2);
        doTest("aab", 2);
        doTest("zyabyz", 1);
        doTest("baabcd", 4);
        doTest("baablkj12345432133d", 9);
    }

    private static void doTest(final String s, int expected) {
        System.out.println(longestPalindrome(s));
    }

    public static int longestPalindrome(final String s) {
        if (Objects.isNull(s) || s.length() < 1) {
            return 0;
        }

        int longest = 1;

        for (int i = 0; i < s.length(); i++) {
            for (int j = i + 1; j < s.length(); j++) {
                String substring = s.substring(i, j + 1);
                String reversed = new StringBuilder(substring).reverse().toString();
                if(reversed.equals(substring) && substring.length() > longest) {
                    longest = substring.length();
                }

            }

        }

        return longest;
    }
}

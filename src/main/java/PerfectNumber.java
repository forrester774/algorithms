public class PerfectNumber {
    public static void main(String[] args) {
        System.out.println(perfectNumber(15));
        System.out.println(perfectNumber(6));

        System.out.println(perfectNumberLooped(15));
        System.out.println(perfectNumberLooped(6));
    }

    public static boolean perfectNumber(int num) {
        if (num < 1) {
            return true;
        }

        int sum = sum(num / 2, num);

        return sum == num;
    }

    private static int sum(int num, int original) {

        if (num == 0) {
            return 0;
        }

        if (original % num == 0) {
            return num + sum(num - 1, original);
        }

        return sum(num - 1, original);
    }

    public static boolean perfectNumberLooped(int num) {
        if (num < 1) {
            return true;
        }

        int result = 0;

        for (int i = 1; i <= num / 2; i++) {
            if (num % i == 0) {
                result += i;
            }
        }

        return num == result;
    }
}

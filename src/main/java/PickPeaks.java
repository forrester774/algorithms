import java.util.*;
import java.util.stream.Collectors;

public class PickPeaks {

    private static String[] msg = {
            "should support finding peaks",
            "should support finding peaks, but should ignore peaks on the edge of the array",
            "should support finding peaks; if the peak is a plateau, it should only return the position of the first element of the plateau",
            "should support finding peaks; if the peak is a plateau, it should only return the position of the first element of the plateau",
            "should support finding peaks, but should ignore peaks on the edge of the array"};

    private static int[][] array = {
            {1, 2, 3, 6, 4, 1, 2, 3, 2, 1},
            {3, 2, 3, 6, 4, 1, 2, 3, 2, 1, 2, 3},
            {3, 2, 3, 6, 4, 1, 2, 3, 2, 1, 2, 2, 2, 1},
            {2, 1, 3, 1, 2, 2, 2, 2, 1},
            {2, 1, 3, 1, 2, 2, 2, 2}};

    private static int[][] posS = {
            {3, 7},
            {3, 7},
            {3, 7, 10},
            {2, 4},
            {2},};

    private static int[][] peaksS = {
            {6, 3},
            {6, 3},
            {6, 3, 2},
            {3, 2},
            {3}};


    public static void main(String[] args) {
        for (int n = 0; n < msg.length; n++) {
            final int[] p1 = posS[n], p2 = peaksS[n];
            Map<String, List<Integer>> expected = new HashMap<>() {{
                put("pos", Arrays.stream(p1).boxed().collect(Collectors.toList()));
                put("peaks", Arrays.stream(p2).boxed().collect(Collectors.toList()));
            }}, actual = getPeaks(array[n]);

            System.out.println(msg[n]);
            System.out.println("Expected");
            System.out.println(expected);
            System.out.println("Actual");
            System.out.println(actual);
        }
    }

    /**
     * In this kata, you will write a function that returns the positions and the values of the "peaks" (or local maxima) of a numeric array.
     * <p>
     * For example, the array arr = [0, 1, 2, 5, 1, 0] has a peak at position 3 with a value of 5 (since arr[3] equals 5).
     * <p>
     * The output will be returned as a ``Map<String,List>with two key-value pairs:"pos"and"peaks".
     * If there is no peak in the given array, simply return {"pos" => [], "peaks" => []}`.
     * <p>
     * Example: pickPeaks([3, 2, 3, 6, 4, 1, 2, 3, 2, 1, 2, 3]) should return {pos: [3, 7], peaks: [6, 3]} (or equivalent in other languages)
     * <p>
     * All input arrays will be valid integer arrays (although it could still be empty), so you won't need to validate the input.
     * <p>
     * The first and last elements of the array will not be considered as peaks
     * (in the context of a mathematical function, we don't know what is after and before and therefore, we don't know if it is a peak or not).
     * <p>
     * Also, beware of plateaus !!! [1, 2, 2, 2, 1] has a peak while [1, 2, 2, 2, 3] and
     * [1, 2, 2, 2, 2] do not. In case of a plateau-peak, please only return the position and value of the beginning of the plateau.
     * For example: pickPeaks([1, 2, 2, 2, 1]) returns {pos: [1], peaks: [2]} (or equivalent in other languages)
     */

    public static Map<String, List<Integer>> getPeaks(int[] arr) {

        Map<String, List<Integer>> result = new HashMap<>();
        result.put("pos", new ArrayList<>());
        result.put("peaks", new ArrayList<>());

        for (int i = 1; i < arr.length - 1; i++) {
            if (arr[i] > arr[i - 1] && arr[i] > arr[i + 1]) {
                result.get("pos").add(i);
                result.get("peaks").add(arr[i]);
            } else if (arr[i] > arr[i - 1] && arr[i] == arr[i + 1]) {
                for (int j = i + 1; j < arr.length - 1; j++) {
                    if (arr[j] < arr[j + 1]) {
                        break;
                    } else if (arr[j] > arr[j + 1]) {
                        result.get("pos").add(i);
                        result.get("peaks").add(arr[i]);
                        break;
                    }
                }
            }
        }

        return result;
    }
}

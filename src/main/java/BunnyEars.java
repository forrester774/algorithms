public class BunnyEars {
    public static void main(String[] args) {
        System.out.println(bunnyEars(3));
        System.out.println(bunnyEarsLoop(3));
    }

    public static int bunnyEars(int num) {
        if (num < 1) {
            return 0;
        }

        int result = 0;
        if (num % 2 == 0) {
            result += 3;
        } else {
            result += 2;
        }

        return result + bunnyEars(num - 1);
    }

    public static int bunnyEarsLoop(int num) {
        int result = 0;

        if (num < 1) {
            return result;
        }

        for (int i = 1; i <= num; i++) {
            if (i % 2 == 0) {
                result += 3;
            } else {
                result += 2;
            }
        }

        return result;
    }
}

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Game {

    public static void main(String[] args) {

        System.out.println(winner(new String[]{"A", "7", "8"}, new String[]{"K", "5", "9"}));
        System.out.println(winner(new String[]{"T"}, new String[]{"T"}));

    }

    /**
     * Steve and Josh are bored and want to play something. They don't want to think too much, so they come up with a really simple game. Write a function called winner and figure out who is going to win.
     * <p>
     * They are dealt the same number of cards. They both flip the card on the top of their deck. Whoever has a card with higher value wins the round and gets one point (if the cards are of the same value, neither of them gets a point). After this, the two cards are discarded and they flip another card from the top of their deck. They do this until they have no cards left.
     * <p>
     * deckSteve and deckJosh are arrays representing their decks. They are filled with cards, represented by a single character. The card rank is as follows (from lowest to highest):
     * <p>
     * '2','3','4','5','6','7','8','9','T','J','Q','K','A'
     * Every card may appear in the deck more than once. Figure out who is going to win and return who wins and with what score:
     * <p>
     * "Steve wins x to y" if Steve wins; where x is Steve's score, y is Josh's score;
     * "Josh wins x to y" if Josh wins; where x is Josh's score, y is Steve's score;
     * "Tie" if the score is tied at the end of the game.
     * Example
     * Steve is dealt: ['A','7','8']
     * Josh is dealt: ['K','5','9']
     * In the first round, ace beats king and Steve gets one point.
     * In the second round, 7 beats 5 and Steve gets his second point.
     * In the third round, 9 beats 8 and Josh gets one point.
     * So you should return: "Steve wins 2 to 1"
     *
     * @param deckSteve
     * @param deckJosh
     * @return
     */

    public static String winner(String[] deckSteve, String[] deckJosh) {
        List<Character> cards = List.of('2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A');

        Map<String, Integer> result = new HashMap<>();
        result.put("Steve", 0);
        result.put("Josh", 0);

        for (int i = 0; i < deckJosh.length; i++) {
            String steveCard = deckSteve[i];
            String joshCard = deckJosh[i];

            int steveCardIndex = cards.indexOf(steveCard.charAt(0));
            int joshCardIndex = cards.indexOf(joshCard.charAt(0));

            if (steveCardIndex > joshCardIndex) {
                Integer steve = result.get("Steve");
                result.put("Steve", ++steve);
            } else if (steveCardIndex < joshCardIndex){
                Integer josh = result.get("Josh");
                result.put("Josh", ++josh);
            }
        }


        Integer steveScore = result.get("Steve");
        Integer joshScore = result.get("Josh");
        StringBuilder builder = new StringBuilder();

        if (steveScore.equals(joshScore)) {
            return "Tie";
        }

        return builder.append(steveScore > joshScore ? "Steve wins " + steveScore + " to " + joshScore : "Josh wins " + joshScore + " to " + steveScore).toString();
    }
}
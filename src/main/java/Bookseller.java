import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Bookseller {

    public static void main(String[] args) {
        String[] art = new String[]{"ABAR 200", "CDXE 500", "BKWR 250", "BTSQ 890", "DRTY 600"};
        String[] cd = new String[]{"A", "B"};
        System.out.println(stockSummary(art, cd)); // (A : 200) - (B : 1140)
    }

    /**
     * A bookseller has lots of books classified in 26 categories labeled A, B, ... Z. Each book has a code c of 3, 4, 5 or more characters.
     * The 1st character of a code is a capital letter which defines the book category.
     * <p>
     * In the bookseller's stocklist each code c is followed by a space and by a positive integer n (int n >= 0) which indicates
     * the quantity of books of this code in stock.
     * <p>
     * For example an extract of a stocklist could be:
     * <p>
     * L = {"ABART 20", "CDXEF 50", "BKWRK 25", "BTSQZ 89", "DRTYM 60"}.
     * or
     * L = ["ABART 20", "CDXEF 50", "BKWRK 25", "BTSQZ 89", "DRTYM 60"] or ....
     * You will be given a stocklist (e.g. : L) and a list of categories in capital letters e.g :
     * <p>
     * M = {"A", "B", "C", "W"}
     * or
     * M = ["A", "B", "C", "W"] or ...
     * and your task is to find all the books of L with codes belonging to each category of M and to sum their quantity according to each category.
     * <p>
     * For the lists L and M of example you have to return the string (in Haskell/Clojure/Racket a list of pairs):
     * <p>
     * (A : 20) - (B : 114) - (C : 50) - (W : 0)
     * where A, B, C, W are the categories, 20 is the sum of the unique book of category A, 114 the sum corresponding to "BKWRK" and "BTSQZ",
     * 50 corresponding to "CDXEF" and 0 to category 'W' since there are no code beginning with W.
     * <p>
     * If L or M are empty return string is "" (Clojure and Racket should return an empty array/list instead).
     * <p>
     * Note:
     * In the result codes and their values are in the same order as in M.
     */

    // 1st parameter is the stocklist (L in example),
    // 2nd parameter is list of categories (M in example)
    public static String stockSummary(String[] lstOfArt, String[] lstOf1stLetter) {

        if(lstOf1stLetter.length == 0 || lstOfArt.length == 0) {
            return "";
        }

        Map<String, Integer> resultSet = new HashMap<>();

        for (String letter : lstOf1stLetter) {
            resultSet.put(letter, 0);
            for (String code : lstOfArt) {
                if (String.valueOf(code.charAt(0)).equals(letter)) {
                    String[] split = code.split(" ");
                    Integer currentValue = resultSet.get(letter);
                    resultSet.put(letter, currentValue + Integer.parseInt(split[1]));
                }

            }
        }

        return resultSet
                .entrySet()
                .stream()
                .map(entry -> "(" + entry.getKey() + " : " + entry.getValue() + ")")
                .collect(Collectors.joining(" - "));
    }
}

import java.util.*;
import java.util.stream.Collectors;

public class HighestScoringWord {

    public static void main(String[] args) {
        System.out.println(high("man i need a taxi up to ubud")); // taxi
        System.out.println(high("what time are we climbing up to the volcano")); // volcano
        System.out.println(high("take me to semynak")); // semynak

        System.out.println(high("aa b")); // aa
        System.out.println(high("b aa")); // b
        System.out.println(high("bb d")); // bb
        System.out.println(high("d bb")); // d
        System.out.println(high("aaa b")); // aaa

    }

    /**
     * Given a string of words, you need to find the highest scoring word.
     * <p>
     * Each letter of a word scores points according to its position in the alphabet: a = 1, b = 2, c = 3 etc.
     * <p>
     * You need to return the highest scoring word as a string.
     * <p>
     * If two words score the same, return the word that appears earliest in the original string.
     * <p>
     * All letters will be lowercase and all inputs will be valid.
     */

    public static String high(String s) {

        if(Objects.isNull(s) || s.isBlank()) {
            return "";
        }

        List<String> alphabet = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
                "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");

        List<String> words = Arrays
                .stream(s.split(" "))
                .collect(Collectors.toList());

        Map<Integer, List<String>> scores = new HashMap<>();

        for (String word : words) {
            int score = 0;
            for (String letter : word.split("")) {
                score += alphabet.indexOf(letter) + 1;
            }
            scores.computeIfAbsent(score, key -> new ArrayList<>()).add(word);
        }

        return scores.entrySet()
                .stream()
                .max(Map.Entry.comparingByKey()).get()
                .getValue()
                .stream()
                .min(Comparator.comparing(words::indexOf))
                .get();
    }


}

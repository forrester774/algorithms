import java.util.ArrayList;
import java.util.List;

public class BinarySearch {

    public static void main(String[] args) {

        Integer[] values = new Integer[1000];
        for (int i = 0; i < 1000; i++) {
            values[i] = i;
        }
        System.out.println(binarySearchRecursive(5, values, 0, values.length - 1));
        System.out.println(binarySearchRecursive(0, values, 0, values.length - 1));
        System.out.println(binarySearchRecursive(234, values, 0, values.length - 1));
        System.out.println(binarySearchRecursive(1001, values, 0, values.length - 1));
        System.out.println(binarySearchRecursive(100000, values, 0, values.length - 1));

        System.out.println("-----------------------------------------------------------------------------");
        System.out.println(binarySearchIterative(5, values, 0, values.length - 1));
        System.out.println(binarySearchIterative(0, values, 0, values.length - 1));
        System.out.println(binarySearchIterative(234, values, 0, values.length - 1));
        System.out.println(binarySearchIterative(1001, values, 0, values.length - 1));
        System.out.println(binarySearchIterative(100000, values, 0, values.length - 1));

    }

    public static boolean binarySearchRecursive(int target, Integer[] array, int start, int end) {
        int middle = (start + end) / 2;
        if (end < start) {
            return false;
        }

        if (target == array[middle]) {
            return true;
        } else if (target < array[middle]) {
            return binarySearchRecursive(target, array, start, middle - 1);
        }

        return binarySearchRecursive(target, array, middle + 1, end);

    }

    public static boolean binarySearchIterative(int target, Integer[] values, int start, int end) {

        while (start <= end) {
            int mid = (start + end) / 2;

            if (target == values[mid]) {
                return true;
            } else if (target < values[mid]) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }

        }
        return false;
    }
}

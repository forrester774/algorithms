import java.util.Objects;

public class SimpleStringIndices {

    public static void main(String[] args) {
        System.out.println(solve("((1)23(45))(aB)", 0)); // 10
        System.out.println(solve("((1)23(45))(aB)", 1)); // 3
        System.out.println(solve("((1)23(45))(aB)", 2)); // -1
        System.out.println(solve("((1)23(45))(aB)", 6)); // 9
        System.out.println(solve("((1)23(45))(aB)", 11)); // 14
        System.out.println(solve("(g(At)IO(f)(tM(qk)YF(n)Nr(E)))", 11)); // 28
        System.out.println(solve("(g(At)IO(f)(tM(qk)YF(n)Nr(E)))", 0)); // 29
        System.out.println(solve("(>_(va)`?(h)C(as)(x(hD)P|(fg)))", 19)); // 22
    }

    /**
     * In this Kata, you will be given a string with brackets and an index of an opening bracket
     * and your task will be to return the index of the matching closing bracket.
     * Both the input and returned index are 0-based except in Fortran where it is 1-based.
     * An opening brace will always have a closing brace. Return -1 if there is no answer
     * (in Haskell, return Nothing; in Fortran, return 0; in Go, return an error)
     * <p>
     * Examples
     * solve("((1)23(45))(aB)", 0) = 10 // the opening brace at index 0 matches the closing brace at index 10
     * solve("((1)23(45))(aB)", 1) = 3
     * solve("((1)23(45))(aB)", 2) = -1 // there is no opening bracket at index 2, so return -1
     * solve("((1)23(45))(aB)", 6) = 9
     * solve("((1)23(45))(aB)", 11) = 14
     * solve("((>)|?(*'))(yZ)", 11) = 14
     * Input will consist of letters, numbers and special characters, but no spaces. The only brackets will be ( and ).
     */

    public static int solve(String str, int index) {

        if (Objects.isNull(str) || str.isBlank() || str.length() <= index) {
            return -1;
        }

        String[] split = str.split("");

        if(!split[index].equals("(")) {
            return -1;
        }

        int closingBracketCount = 0;

        for (int i = index + 1; i < split.length; i++) {

            if (split[i].equals("(")) {
                closingBracketCount++;
            }

            if (split[i].equals(")") && closingBracketCount == 0) {
                return i;
            } else if(split[i].equals(")") && closingBracketCount > 0) {
                closingBracketCount--;
            }
        }

        return -1;
    }
}

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Scramblies {

    public static void main(String[] args) {
        System.out.println(Scramblies.scramble("rkqodlw","world"));//, true);
        System.out.println(Scramblies.scramble("cedewaraaossoqqyt","codewars"));//,true);
        System.out.println(Scramblies.scramble("katas","steak"));//,false);
        System.out.println(Scramblies.scramble("scriptjavx","javascript"));//,false);
        System.out.println(Scramblies.scramble("scriptingjava","javascript"));//true);
        System.out.println(Scramblies.scramble("scriptsjava","javascripts"));//,true);
        System.out.println(Scramblies.scramble("javscripts","javascript"));//,false);
        System.out.println(Scramblies.scramble("aabbcamaomsccdd","commas"));//,true);
        System.out.println(Scramblies.scramble("commas","commas"));//,true);
        System.out.println(Scramblies.scramble("sammoc","commas"));//,true);

    }

    /**
     * Complete the function scramble(str1, str2) that returns true if a portion of str1 characters can be rearranged to match str2, otherwise returns false.
     *
     * Notes:
     *
     * Only lower case letters will be used (a-z). No punctuation or digits will be included.
     * Performance needs to be considered.
     * Examples
     * scramble('rkqodlw', 'world') ==> True
     * scramble('cedewaraaossoqqyt', 'codewars') ==> True
     * scramble('katas', 'steak') ==> False
     */

    public static boolean scramble(String str1, String str2) {
        List<String> letters = Arrays.stream(str1.split("")).collect(Collectors.toList());

        return Arrays.stream(str2.split("")).allMatch(letter -> {
            if(letters.contains(letter)) {
                letters.remove(letter);
                return true;
            }
            return false;
        });
    }
}
